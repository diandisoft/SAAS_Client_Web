/**
 * Created by jianbo on 2016/1/19.
 * 公司相关逻辑服务
 */

define(['app'], function (app) {

    app.factory('companyService', ['globalConfig','$http','CacheFactory','$q', function (globalConfig,$http,CacheFactory,$q) {
        var serviceFactory = {};
        serviceFactory.getDepartment = _getDepartment;
        serviceFactory.getCompanyMemberList = _getCompanyMemberList;
        serviceFactory.comfirmMember = _comfirmMember;
        serviceFactory.getRoleList = _getRoleList;
        serviceFactory.getRole = _getRole;
        serviceFactory.setDimission = _setDimission;
        serviceFactory.addMember = _addMember;
        serviceFactory.exitMemberRole = _exitMemberRole;
        serviceFactory.exitMember = _exitMember;
        serviceFactory.paramMember = _paramMember;
        serviceFactory.sendSMS = _sendSMS;
        serviceFactory.transferDepartment = _transferDepartment;
        serviceFactory.exitDepartment = _exitDepartment;
        serviceFactory.addDepartment = _addDepartment;
        serviceFactory.getDepartmentInfo = _getDepartmentInfo;
        serviceFactory.deleteDepartment = _deleteDepartment;
        serviceFactory.transferDepartmentForOne = _transferDepartmentForOne;
        serviceFactory.downloadCvs = _downloadCvs;
        serviceFactory.sendCvs = _sendCvs;
        serviceFactory.deleteMember = _deleteMember;
        serviceFactory.exportCvs = _exportCvs;
        serviceFactory.dissolutionCompany = _dissolutionCompany;
        serviceFactory.setFunctionRange = _setFunctionRange;
        serviceFactory.setMemberArchives = _setMemberArchives;
        serviceFactory.addRole = _addRole;
        serviceFactory.deleteRole = _deleteRole;
        serviceFactory.exitRole = _exitRole;
        serviceFactory.batchJoinRole = _batchJoinRole;
        serviceFactory.batchOutRole = _batchOutRole;
        serviceFactory.outCompany = _outCompany;
        serviceFactory.getRoleFunction = _getRoleFunction;
        serviceFactory.setRoleFunction = _setRoleFunction;
        serviceFactory.getImagesCode = _getImagesCode;
        serviceFactory.getFileUpload = _getFileUpload;

        /**
         * 公司id获取门店list
         * @param companyId 公司id
         * @private
         */
        function _getDepartment(companyId){
            var deferred = $q.defer();
            var url = globalConfig.apiUrl.company.getDepartmentList.format(companyId);
            $http.get(url).success(function(response){
                deferred.resolve(response);
            }).error(function(err){
                deferred.reject(err)
            });
            return deferred.promise;
        }

        /**
         * 获取员工list
         * @private
         */
        function _getCompanyMemberList(data){
            var deferred = $q.defer();
            var url = globalConfig.apiUrl.company.getCompanyMemberList.format(data.CompanyId);
            $http({
                method:'GET',
                url:url,
                params:data
            }).success(function(response){
                deferred.resolve(response);
            }).error(function(err){
                deferred.reject(err)
            });
            return deferred.promise;
        }
        /**
         * 获取全部角色
         * @param companyId 公司id
         * @private
         */
        function _getRoleList(companyId){
            var deferred = $q.defer();
            var url = globalConfig.apiUrl.company.getRoleList.format(companyId);
            $http.get(url).success(function(response){
                deferred.resolve(response);
            }).error(function(err){
                deferred.reject(err)
            });
            return deferred.promise;
        }
        /**
         * 获取角色信息
         * @param companyId 公司id
         * @private
         */
        function _getRole(data){
            var deferred = $q.defer();
            var url = globalConfig.apiUrl.company.getRole.format(data.CompanyId,data.RoleId);
            $http({
                method:'GET',
                url:url,
                params:data
            }).success(function(response){
                deferred.resolve(response);
            }).error(function(err){
                deferred.reject(err)
            });
            return deferred.promise;
        }
        /**
         * 确认审核员工
         * @param companyId 公司id
         * @private
         */
        function _comfirmMember(data){
            var deferred = $q.defer();
            var url = globalConfig.apiUrl.company.comfirmMember.format(data.CompanyId,data.MemberId);
            $http.put(url,data).success(function(response){
                deferred.resolve(response);
            }).error(function(err){
                deferred.reject(err)
            });
            return deferred.promise;
        }
        /**
         * 批量离职成员
         * @param companyId 公司id
         * @private
         */
        function _setDimission(data){
            var deferred = $q.defer();
            var url = globalConfig.apiUrl.company.setDimission.format(data.CompanyId);
            $http({
                method:'DELETE',
                    url:url,
                params:data
            }).success(function(response){
                deferred.resolve(response);
            }).error(function(err){
                deferred.reject(err)
            });
            return deferred.promise;
        }
        /**
         * 管理员创建成员
         * @param companyId 公司id
         * @private
         */
        function _addMember(data){
            var deferred = $q.defer();
            var url = globalConfig.apiUrl.company.addMember.format(data.CompanyId);
            $http.post(url,data).success(function(response){
                deferred.resolve(response);
            }).error(function(err){
                deferred.reject(err)
            });
            return deferred.promise;
        }
        /**
         * 管理员设置角色
         * @param companyId 公司id
         * @private
         */
        function _exitMemberRole(data){
            var deferred = $q.defer();
            var url = globalConfig.apiUrl.company.exitMemberRole.format(data.CompanyId,data.MemberId);
            $http.put(url,data).success(function(response){
                deferred.resolve(response);
            }).error(function(err){
                deferred.reject(err)
            });
            return deferred.promise;
        }
        /**
         * 管理员编辑成员
         * @param companyId 公司id
         * @private
         */
        function _exitMember(data){
            var deferred = $q.defer();
            var url = globalConfig.apiUrl.company.exitMember.format(data.CompanyId,data.MemberId);
            $http.put(url,data).success(function(response){
                deferred.resolve(response);
            }).error(function(err){
                deferred.reject(err)
            });
            return deferred.promise;
        }
        /**
         * 管理员获取成员
         * @param companyId 公司id
         * @private
         */
        function _paramMember(data){
            var deferred = $q.defer();
            var url = globalConfig.apiUrl.company.paramMember.format(data.CompanyId,data.MemberId);
            $http.get(url).success(function(response){
                deferred.resolve(response);
            }).error(function(err){
                deferred.reject(err)
            });
            return deferred.promise;
        }

        /**
         *发送邀请短信
         * @param companyId 公司id
         **/
        function _sendSMS(data){
            var deferred = $q.defer();
            var url = globalConfig.apiUrl.company.sendSMS.format(data.CompanyId,data.MemberId);
            $http.get(url).success(function(response){
                deferred.resolve(response);
            }).error(function(err){
                deferred.reject(err)
            });
            return deferred.promise;
        }

        /**
         *批量转移部门
         **/
        function  _transferDepartment(data){
            var deferred = $q.defer();
            var url = globalConfig.apiUrl.company.transferDepartment.format(data.CompanyId,data.DepartmentId);
            $http.put(url,data).success(function(response){
                deferred.resolve(response);
            }).error(function(err){
                deferred.reject(err)
            });
            return deferred.promise;
        }

        /**
         *编辑部门
         **/
        function  _exitDepartment(data){
            var deferred = $q.defer();
            var url = globalConfig.apiUrl.company.exitDepartment.format(data.CompanyId,data.DepartmentId);
            $http.put(url,data).success(function(response){
                deferred.resolve(response);
            }).error(function(err){
                deferred.reject(err)
            });
            return deferred.promise;
        }

        /**
         *新增部门
         **/
        function  _addDepartment(data){
            var deferred = $q.defer();
            var url = globalConfig.apiUrl.company.addDepartment.format(data.CompanyId);
            $http.post(url,data).success(function(response){
                deferred.resolve(response);
            }).error(function(err){
                deferred.reject(err)
            });
            return deferred.promise;
        }

        /**
         *获取部门信息
         **/
        function  _getDepartmentInfo(data){
            var deferred = $q.defer();
            var url = globalConfig.apiUrl.company.getDepartmentInfo.format(data.CompanyId,data.DepartmentId);
            $http({
                method:'GET',
                url:url,
                params:data
            }).success(function(response){
                deferred.resolve(response);
            }).error(function(err){
                deferred.reject(err)
            });
            return deferred.promise;
        }

        /**
         *解散部门
         **/
        function  _deleteDepartment(data){
            var deferred = $q.defer();
            var url = globalConfig.apiUrl.company.deleteDepartment.format(data.CompanyId,data.DepartmentId);
            $http({
                method:'DELETE',
                url:url,
                params:data
            }).success(function(response){
                deferred.resolve(response);
            }).error(function(err){
                deferred.reject(err)
            });
            return deferred.promise;
        }

        /**
         *编辑成员部门
         **/
        function  _transferDepartmentForOne(data){
            var deferred = $q.defer();
            var url = globalConfig.apiUrl.company.transferDepartmentForOne.format(data.CompanyId,data.MemberId);
            $http.put(url,data).success(function(response){
                deferred.resolve(response);
            }).error(function(err){
                deferred.reject(err)
            });
            return deferred.promise;
        }

        /**
         *下载模板
         * @param companyId 公司id
         **/
        function _downloadCvs(data){
            var deferred = $q.defer();
            var url = globalConfig.apiUrl.company.downloadCvs.format(data.CompanyId);
            $http.get(url).success(function(response){
                deferred.resolve(response);
            }).error(function(err){
                deferred.reject(err)
            });
            return deferred.promise;
        }
        /**
         *上传Cvs
         * @param companyId 公司id
         **/
        function _sendCvs(data){
            var deferred = $q.defer();
            var url = globalConfig.apiUrl.company.sendCvs.format(data.CompanyId);
            $http.post(url,data).success(function(response){
                deferred.resolve(response);
            }).error(function(err){
                deferred.reject(err)
            });
            return deferred.promise;
        }
        /**
         *导出Cvs
         * @param companyId 公司id
         **/
        function _exportCvs(data){
            var deferred = $q.defer();
            var url = globalConfig.apiUrl.company.exportCvs.format(data.CompanyId);
            $http({
                method:'GET',
                url:url,
                params:data
            }).success(function(response){
                deferred.resolve(response);
            }).error(function(err){
                deferred.reject(err)
            });
            return deferred.promise;
        }
        /**
         * 管理员删除成员
         * @param companyId 公司id
         * @private
         */
        function _deleteMember(data){
            var deferred = $q.defer();
            var url = globalConfig.apiUrl.company.deleteMember.format(data.CompanyId,data.MemberId);
            $http({
                method:'DELETE',
                url:url,
                params:data
            }).success(function(response){
                deferred.resolve(response);
            }).error(function(err){
                deferred.reject(err)
            });
            return deferred.promise;
        }
        /**
         *解散公司
         * @param companyId 公司id
         **/
        function _dissolutionCompany(data){
            var deferred = $q.defer();
            var url = globalConfig.apiUrl.company.dissolutionCompany.format(data.CompanyId);
            $http({
                method:'DELETE',
                url:url,
                params:data
            }).success(function(response){
                deferred.resolve(response);
            }).error(function(err){
                deferred.reject(err)
            });
            return deferred.promise;
        }
        /**
         *设置职权范围
         * @param companyId 公司id
         **/
        function _setFunctionRange(data){
            var deferred = $q.defer();
            var url = globalConfig.apiUrl.company.setFunctionRange.format(data.CompanyId,data.MemberId);
            $http.put(url,data).success(function(response){
                deferred.resolve(response);
            }).error(function(err){
                deferred.reject(err)
            });
            return deferred.promise;
        }
        /**
         *设置电子档案
         * @param companyId 公司id
         **/
        function _setMemberArchives(data){
            var deferred = $q.defer();
            var url = globalConfig.apiUrl.company.setMemberArchives.format(data.CompanyId,data.MemberId);
            $http.put(url,data).success(function(response){
                deferred.resolve(response);
            }).error(function(err){
                deferred.reject(err)
            });
            return deferred.promise;
        }
        /**
         *添加角色
         * @param companyId 公司id
         **/
        function _addRole(data){
            var deferred = $q.defer();
            var url = globalConfig.apiUrl.company.addRole.format(data.CompanyId,data.RoleId);
            $http.post(url,data).success(function(response){
                deferred.resolve(response);
            }).error(function(err){
                deferred.reject(err)
            });
            return deferred.promise;
        }
        /**
         *编辑角色
         * @param companyId 公司id
         **/
        function _exitRole(data){
            var deferred = $q.defer();
            var url = globalConfig.apiUrl.company.exitRole.format(data.CompanyId,data.RoleId);
            $http.put(url,data).success(function(response){
                deferred.resolve(response);
            }).error(function(err){
                deferred.reject(err)
            });
            return deferred.promise;
        }
        /**
         *删除角色
         * @param companyId 公司id
         **/
        function _deleteRole(data){
            var deferred = $q.defer();
            var url = globalConfig.apiUrl.company.deleteRole.format(data.CompanyId,data.RoleId);
            $http({
                method:'DELETE',
                url:url,
                params:data
            }).success(function(response){
                deferred.resolve(response);
            }).error(function(err){
                deferred.reject(err)
            });
            return deferred.promise;
        }
        /**
         *批量成员加入角色
         * @param companyId 公司id
         **/
        function _batchJoinRole(data){
            var deferred = $q.defer();
            var url = globalConfig.apiUrl.company.batchJoinRole.format(data.CompanyId,data.RoleId);
            $http.post(url,data).success(function(response){
                deferred.resolve(response);
            }).error(function(err){
                deferred.reject(err)
            });
            return deferred.promise;
        }
        /**
         *批量成员退出角色
         * @param companyId 公司id
         **/
        function _batchOutRole(data){
            var deferred = $q.defer();
            var url = globalConfig.apiUrl.company.batchOutRole.format(data.CompanyId,data.RoleId);
            $http({
                method:'DELETE',
                url:url,
                params:data
            }).success(function(response){
                deferred.resolve(response);
            }).error(function(err){
                deferred.reject(err)
            });
            return deferred.promise;
        }
        /**
         *退出公司
         * @param companyId 公司id
         **/
        function _outCompany(data){
            var deferred = $q.defer();
            var url = globalConfig.apiUrl.company.outCompany.format(data.CompanyId,data.MemberId);
            $http({
                method:'DELETE',
                url:url,
                params:data
            }).success(function(response){
                deferred.resolve(response);
            }).error(function(err){
                deferred.reject(err)
            });
            return deferred.promise;
        }
        /**
        *获取角色功能权限
        **/
        function _getRoleFunction(data){
            var deferred = $q.defer();
            var url = globalConfig.apiUrl.company.getRoleFunction.format(data.CompanyId,data.RoleId);
            $http.get(url).success(function(response){
                deferred.resolve(response);
            }).error(function(err){
                deferred.reject(err)
            });
            return deferred.promise;
        }

        /**
         *设置角色功能权限
         **/
        function _setRoleFunction(data){
            var deferred = $q.defer();
            var url = globalConfig.apiUrl.company.setRoleFunction.format(data.CompanyId,data.RoleId);
            $http.put(url,data).success(function(response){
                deferred.resolve(response);
            }).error(function(err){
                deferred.reject(err)
            });
            return deferred.promise;
        }
        /*
        获取图形验证码
        */
        function _getImagesCode(data){
            var deferred = $q.defer();
            var text = Math.random();
            var url = globalConfig.apiUrl.company.getImagesCode.format(data,text);
            deferred.resolve(url);
            return deferred.promise;
        }
        /*
         获取文件上传
         */
        CacheFactory('headerPortraitCache', {
            maxAge: 15 * 60 * 1000, // Items added to this cache expire after 15 minutes
            cacheFlushInterval: 60 * 60 * 1000, // This cache will clear itself every hour
            deleteOnExpire: 'aggressive' // Items will be deleted from this cache when they expire
        });
        function _getFileUpload(type){
            var deferred = $q.defer();
            var start = new Date().getTime();
            var url = globalConfig.apiUrl.company.fileUpload.format(type);
            $http.get(url,{
                cache: CacheFactory.get('headerPortraitCache')
            }).success(function(response){
                console.log('time taken for request: ' + (new Date().getTime() - start) + 'ms');
                deferred.resolve(response);
            }).error(function(err){
                deferred.reject(err)
            });
            return deferred.promise;
        }

        return serviceFactory;
    }]);
});