/**
 * Created by jianbo on 2016/3/9.
 * 个人设置——头像设置
 */
'use strict';

define(['app',"../companyService"],function (app) {
    app.controller('individualSetIconController',controlSetIcon);
    controlSetIcon.$inject = ['globalConfig','$scope','companyService','dictionaryService'];
    /*个人头像设置模块*/
    function controlSetIcon(globalConfig,$scope,companyService,dictionaryService) {
        var file;
        var accessid = '';
        var accesskey = '';
        var host = '';
        var policyBase64 = '';
        var signature = '';
        var callbackbody = '';
        var filename = '';
        var key = '';
        var rid = 0;
        var expire = 0;
        var now = Date.parse(new Date()) / 1000;

        $scope.myImage='';
        $scope.myCroppedImage='';
        $scope.cropType="circle";

        function get_signature(obj)
        {
            //可以判断当前expire是否超过了当前时间,如果超过了当前时间,就重新取一下.3s 做为缓冲
            now = Date.parse(new Date()) / 1000;
            console.log('get_signature ...');
            console.log('expire:' + expire.toString());
            console.log('now:', + now.toString());
            if (expire < now + 3)
            {
                host = obj['host'];
                policyBase64 = obj['policy'];
                accessid = obj['accessid'];
                signature = obj['signature'];
                expire = parseInt(obj['expire']);
                callbackbody = obj['callback'];
                //key = obj['dir']
                key = obj['filename'];
                rid = obj['rid'];
                return true;
            }
            return false;
        }
        function set_upload_param(up,data)
        {
            var ret = get_signature(data);
            if (ret == true)
            {
                var new_multipart_params = {
                    'key' : key,
                    'policy': policyBase64,
                    'OSSAccessKeyId': accessid,
                    'success_action_status' : '200', //让服务端返回200,不然，默认会返回204
                    'signature': signature
                };

                up.setOption({
                    'url': host,
                    'multipart_params': new_multipart_params
                });
                console.log('reset uploader');
            }
        }
        var uploader = new plupload.Uploader({
            runtimes : 'html5,flash,silverlight,html4',
            browse_button : 'selectfiles',
            container: document.getElementById('container'),
            flash_swf_url : 'public/bower_components/Plupload/js/Moxie.swf',
            silverlight_xap_url : 'public/bower_components/Plupload/js/Moxie.xap',

            url : 'http://oss.aliyuncs.com',

            init: {
                PostInit: function() {
                    document.getElementById('postfiles').onclick = function() {
                        file = dataURLtoBlob($scope.myCroppedImage);
                        uploader.addFile(file,'touxiang');
                        var fileDirectory = dictionaryService.FileDirectory();
                        companyService.getFileUpload(fileDirectory[2].Value).then(function(data) {
                            set_upload_param(uploader,data);
                            uploader.start();
                        });
                    };
                },

                FileUploaded: function(up, file, info) {
                    console.log('uploaded');
                    console.log(info.status);
                    if (info.status == 200)
                    {
                        globalConfig.rootScope.successMessage = '上传成功';
                        globalConfig.rootScope.errorMessage = '';
                        /*重新获取头像*/
                        globalConfig.rootScope.getHeadPortrait();
                    }
                    else
                    {
                        globalConfig.rootScope.successMessage = '';
                        globalConfig.rootScope.errorMessage = '上传失败';
                    }
                },

                Error: function(up, err) {
                    //set_upload_param(up);
                    document.getElementById('console').appendChild(document.createTextNode("\nError xml:" + err.response));
                }
            }
        });

        uploader.init();


        var handleFileSelect=function(evt) {
            var file=evt.currentTarget.files[0];
            var reader = new FileReader();
            reader.onload = function (evt) {
                $scope.$apply(function($scope){
                    $scope.myImage=evt.target.result;
                });
            };
            reader.readAsDataURL(file);
        };
        /*dataUrl转Blob*/
        function dataURLtoBlob(dataurl) {
            var arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
                bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
            while(n--){
                u8arr[n] = bstr.charCodeAt(n);
            }
            return new Blob([u8arr], {type:mime});
        }
        angular.element(document.querySelector('#fileInput')).on('change',handleFileSelect);
    }

});