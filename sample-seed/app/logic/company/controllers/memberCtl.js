/**
 * Created by jianbo on 2016/1/30.
 * 成员详情模板页
 */
'use strict';

define(['app',"../companyService"],function (app) {
    /*设为离职弹出款Ctl*/
    app.controller('ModalSetDimissionCtrl',controlSetDimission);
    controlSetDimission.$inject = ['globalConfig','$scope','$uibModalInstance','modalData','$validation','currentUserService','$modal','companyService','$timeout'];
    /*选择其他成员*/
    app.controller('ModalSelectOtherMemberCtrl',controlSelectOtherMember);
    controlSelectOtherMember.$inject = ['globalConfig','$scope','$uibModalInstance','modalData','currentUserService','companyService','$timeout'];
    app.controller('MemberController',control);
    control.$inject = ['globalConfig','$scope','cryptoJS','companyService','$validation','$stateParams','$modal','currentUserService','dictionaryService'];
    function controlSetDimission(globalConfig,$scope, $uibModalInstance,modalData,$validationProvider,currentUserService,$modal,companyService,$timeout) {
        var currentUser;
        $scope.title = "设为离职";
        $scope.checkValid = $validationProvider.checkValid;
        $scope.selectMember = _selectMember;
        $scope.form = {
            people:'',
            code:''
        };
        function _selectMember(){
            var modal_data = {
                controller:{
                    name:''
                }
            };
            var modal_url = "modalSelectOtherMember.html";
            var modal_ctl = "ModalSelectOtherMemberCtrl";
            modal_data.controller.name = '选择转授人';
            var size = 'lg';
            var modalInstance = $modal.open({
                animation: true,
                templateUrl: modal_url,
                controller: modal_ctl,
                size: size,
                backdrop:'static',
                resolve: {
                    modalData: function () {
                        return modal_data;
                    }
                }
            });
            modalInstance.result.then(function (data) {
                $scope.form.people = data.i;
            }, _error);
        }
            $scope.ok = function (form) {
                modalData.TransferId = form.people.id;
                modalData.Code = form.code;
                var deleteMember =  companyService.deleteMember(modalData);
                deleteMember.then(_success,_error);

            };
            $scope.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        function _success(){
            globalConfig.rootScope.successMessage = '操作成功';
            globalConfig.rootScope.errorMessage = '';
            $timeout(function () {
                $uibModalInstance.close();
            }, 2000);
        }
        function _error(err){
            if(err) {
                globalConfig.rootScope.successMessage = '';
                globalConfig.rootScope.errorMessage = err.message.join('&lt;br&frasl;&gt;');
            }
        }
    }
    function controlSelectOtherMember(globalConfig,$scope, $uibModalInstance,modalData,currentUserService,companyService,$timeout) {
        var tree,Department,currentUser,member;
        /*成员列表初始条件*/
        var paramMemberCondition = {
            CompanyId:" ",
            Name:" ",
            Phone:" ",
            Position:" ",
            RoleId:" ",
            DepartmentId:" ",
            Status:"2",
            IsDimission:"false",
            SortOrder:"0",
            CurrentPage:1,
            PageSize:10
        };
        $scope.numPages = 5;//分页显示数量
        $scope.pageSize = paramMemberCondition.PageSize;
        $scope.doing_async1 = true;
        $scope.depList=[];
        $scope.my_tree1 = tree = {};
        $scope.title = modalData.controller.name;
        $scope.myTreeHandler1 = _myTreeHandler1;
        $scope.changePageHandler = _changePageHandler;
        $scope.selectMember = _selectMember;
        $scope.byNameHandler1 = _byNameHandler1;
        $scope.byRoleHandler1 = _byRoleHandler1;
        init();

        function init() {
            currentUserService.currentUser().then(function(data){
                currentUser = data;
                paramMemberCondition.CompanyId = currentUser.company.id;
                paramMemberCondition.Position = " ";
                _getRolt(currentUser.company.id);
                _getDepartmentList();
            },_error);
        }

        function _getDepartmentList(){
            var list = companyService.getDepartment(currentUser.company.id);
            list.then(
                function (data) {
                    $scope.depList = data.tree;
                    $scope.doing_async1 = false;
                    return $timeout(
                        function() {
                            tree.select_first_branch();
                            tree.expand_all();
                        },300);
                }
            );
        }

        function _myTreeHandler1 (branch) {
            paramMemberCondition.DepartmentId = branch.depId;
            paramMemberCondition.IsDimission = 'false';
            Department = branch;
            $scope.depName = branch.label;
            paramMemberCondition.CurrentPage = 1;
            _getMemberList();
        }

        /*姓名\电话\职位搜索*/
        function _byNameHandler1(vm) {
            if(vm === undefined||vm.people_name === undefined||vm.people_name==''){
                paramMemberCondition.Name=" ";
            }else {
                paramMemberCondition.Name = vm.people_name.replace(/[ ]/g,"");
            }
            if(vm === undefined||vm.people_phone === undefined||vm.people_phone==''){
                paramMemberCondition.Phone=" ";
            }else {
                paramMemberCondition.Phone = vm.people_phone.replace(/[ ]/g,"");
            }
            if(vm === undefined||vm.people_position === undefined||vm.people_position==''){
                paramMemberCondition.Position=" ";
            }else {
                paramMemberCondition.Position = vm.people_position.replace(/[ ]/g,"");
            }
            paramMemberCondition.CurrentPage = 1;
            _getMemberList();
        };
        /*角色搜索*/
        function _byRoleHandler1(selected){
            if(selected!=null) {
                paramMemberCondition.RoleId = selected.id;
            }else{
                paramMemberCondition.RoleId = " ";
            }
            paramMemberCondition.CurrentPage = 1;
            _getMemberList();
        }
        function _getRolt(cid) {
            var getRoltList = companyService.getRoleList(cid);
            getRoltList.then(function (data) {
                $scope.roles = data.list;
            }, _error);
        }
        /*获取成员列表*/
        function _getMemberList(){
            var memberList = companyService.getCompanyMemberList(paramMemberCondition);
            memberList.then(function(data){
                $scope.MemberList = data.list;
                $scope.bigTotalItems = data.listcount;
            },_error);
        }
        /*翻页*/
        function _changePageHandler() {
            paramMemberCondition.CurrentPage = this.P;
            _getMemberList();
        }
        /*选择成员*/
        function _selectMember(){
            $scope.selected = this;
        }
        function _error(err){
            if(err) {
                globalConfig.rootScope.successMessage = '';
                globalConfig.rootScope.errorMessage = err.message.join('&lt;br&frasl;&gt;');
            }
        }
        $scope.ok = function () {
            $uibModalInstance.close($scope.selected);
        };
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }
    function control(globalConfig,$scope,cryptoJS,companyService,$validationProvider,$stateParams,$modal,currentUserService,dictionaryService) {

        var data = {};
        var currentUser;
        init();
        function init() {
            currentUserService.currentUser().then(function (response) {
                currentUser = response;
                var id = $stateParams.data;
                _getMemberInfo(id);
            }, _error);
        }

        function _getMemberInfo(id){
            data.CompanyId = currentUser.company.id;
            data.MemberId = id;
            var getMember = companyService.paramMember(data);
            getMember.then(
                function(data){
                    $scope.user = data.memberinfo;
                    var status = $scope.user.status;
                    $scope.user.gender = dictionaryService.Gender()[$scope.user.gender].Description;
                    if ($scope.user.roleList.length <= 0) {
                        $scope.user.roleList = {'name': '未分配'}
                    }
                },_error);
        }

        $scope.open = function(size) {
            var modalInstance = $modal.open({
                animation: false,
                templateUrl: 'modalSetDimission.html',
                controller: 'ModalSetDimissionCtrl',
                size: size,
                backdrop:'static',
                resolve: {
                    modalData: function () {
                        return data;
                    }
                }
            });

            modalInstance.result.then(function (form) {
                init();
            }, _error);
        };

        function _success(){
            globalConfig.rootScope.successMessage = '操作成功';
            globalConfig.rootScope.errorMessage = '';
        }
        function _error(err){
            if(err) {
                globalConfig.rootScope.successMessage = '';
                globalConfig.rootScope.errorMessage = err.message.join('&lt;br&frasl;&gt;');
            }
        }
    }
});