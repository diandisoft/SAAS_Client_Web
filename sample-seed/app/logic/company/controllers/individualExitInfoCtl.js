/**
 * Created by jianbo on 2016/3/9.
 * 个人设置——个人信息编辑
 */
'use strict';

define(['app',"../companyService"],function (app) {
    app.controller('individualExitInfoController',controlExitInfo);
    controlExitInfo.$inject = ['globalConfig','$scope','cryptoJS','companyService','$validation','$modal','currentUserService','dictionaryService'];
    /*个人信息编辑模块*/
    function controlExitInfo(globalConfig,$scope,cryptoJS,companyService,$validationProvider,$modal,currentUserService,dictionaryService) {
        var data = {
            CompanyId:'',
            MemberId:''
        };
        $scope.popup = {
            opened: false
        };
        $scope.today =  new Date();
        $scope.openDate = _openDate;
        $scope.exitInfo = _exitInfo;
        $scope.checkValid = $validationProvider.checkValid;
        init();
        function init() {
            currentUserService.currentUser().then(function (currentUser) {
                data.CompanyId = currentUser.company.id;
                data.MemberId = currentUser.id;
                companyService.paramMember(data).then(
                    function(data){
                        $scope.user = data.memberinfo;
                        if($scope.user.birthday !== undefined) {
                            $scope.user.birthday = $scope.user.birthday.toDate();
                        }
                        if($scope.user.createTime !== undefined) {
                            $scope.CreateTime = $scope.user.createTime.toDate();
                        }
                        $scope.DepartmentName = $scope.user.departmentList[0].name;
                        $scope.Tel = $scope.user.tel1;
                    },_error);
            }, _error);
        }
        /*打开选择日期*/
        function _openDate() {
            $scope.popup.opened = true;
        }
        /*编辑个人信息*/
        function _exitInfo(user) {
            var roles = [];
            user.CompanyId = data.CompanyId;
            user.MemberId = user.id;
            var exitMember = companyService.exitMember(user);
            angular.forEach($scope.user.roleList,function(value,key){
                roles.push(value.id);
            });
            exitMember.then(
                function(){
                    _success('操作成功');
                    /*传值to-parent给父级controller，父级controller更新数据*/
                    $scope.$emit('to-parent', 'parent');
                    init();
                },_error);
        }

        function _success(){
            globalConfig.rootScope.successMessage = '操作成功';
            globalConfig.rootScope.errorMessage = '';
        }
        function _error(err){
            if(err) {
                globalConfig.rootScope.successMessage = '';
                globalConfig.rootScope.errorMessage = err.message.join('&lt;br&frasl;&gt;');
            }
        }
    }

});