/**
 * Created by jianbo on 2016/1/19.
 * 角色管理
 */

'use strict';

define(['app',"../companyService"],function (app) {
    /*添加角色与编辑角色操作弹出款Ctl*/
    app.controller('ModalAddRoleCtrl',controlAddRole);
    controlAddRole.$inject = ['globalConfig','$scope','$uibModalInstance','$validation','currentUserService','modalData','$timeout','companyService'];
    /*删除角色弹出款Ctl*/
    app.controller('ModalDelectRoleCtrl',controlDelectRole);
    controlDelectRole.$inject = ['globalConfig','$scope','$uibModalInstance','modalData','$validation','currentUserService','companyService','$timeout'];
    /*编辑权限弹出款Ctl*/
    app.controller('ModalSetRoleFunctionCtrl',controlSetRoleFunction);
    controlSetRoleFunction.$inject = ['globalConfig','$scope','$uibModalInstance','modalData','companyService','$timeout','dictionaryService'];

    app.controller('RoleController',control);
    control.$inject = ['globalConfig','$scope','$state','companyService','$timeout','$modal','currentUserService'];

    function controlAddRole(globalConfig,$scope, $uibModalInstance,$validationProvider,currentUserService,modalData,$timeout,companyService) {
        $scope.checkValid = $validationProvider.checkValid;
        $scope.title = modalData.title;
            if(modalData.role!=undefined){
                $scope.form={};
                $scope.form.name = modalData.role.name;
            }
            $scope.ok = function (form) {
                currentUserService.currentUser().then(function(currentUser){
                    form.CompanyId = currentUser.company.id;
                    var roleService;
                    if(modalData.role === undefined) {
                        roleService = companyService.addRole(form);
                    }else{
                        form.RoleId = modalData.role.id;
                        roleService = companyService.exitRole(form);
                    }
                    roleService.then(function () {
                        if(modalData.role === undefined) {
                            _success('成功添加角色');
                        }else{
                            _success('成功编辑角色');
                        }
                        $timeout(function () {
                            $uibModalInstance.close(form);
                        }, 2000);
                    }, _error);
                },_error);
            };
            $scope.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        function _success(msg){
            globalConfig.rootScope.successMessage = msg;
            globalConfig.rootScope.errorMessage = '';
        }
        function _error(err){
            if(err) {
                globalConfig.rootScope.successMessage = '';
                globalConfig.rootScope.errorMessage = err.message.join('&lt;br&frasl;&gt;');
            }
        }
    }
    function controlDelectRole(globalConfig,$scope, $uibModalInstance,modalData,$validationProvider,currentUserService,companyService,$timeout) {
        $scope.title = modalData.controller.name;
        $scope.message = modalData.controller.message;
        $scope.form = {};
        $scope.checkValid = $validationProvider.checkValid;
            $scope.ok = function (form) {
                currentUserService.currentUser().then(function(currentUser){
                    form.CompanyId = currentUser.company.id;
                    form.RoleId = modalData.role.id;
                    var getRoleList = companyService.deleteRole(form);
                    getRoleList.then(function(){
                        _success('成功删除角色');
                        $timeout(function () {
                            $uibModalInstance.close(form);
                        }, 2000);
                    },_error);
                },_error);
            };
            $scope.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };

        function _success(msg){
            globalConfig.rootScope.successMessage = msg;
            globalConfig.rootScope.errorMessage = '';
        }
        function _error(err){
            if(err) {
                globalConfig.rootScope.successMessage = '';
                globalConfig.rootScope.errorMessage = err.message.join('&lt;br&frasl;&gt;');
            }
        }
    }
    function controlSetRoleFunction(globalConfig,$scope, $uibModalInstance,modalData,companyService,$timeout,dictionaryService) {
        $scope.FunctionList = [];
        $scope.title = modalData.controller.name;
        $scope.RoleInfo = modalData.Role;
        $scope.form = {};
        $scope.isChecked = _isChecked;
        init();
        function init(){
            _getRoleFunction();
        }
        function _getRoleFunction(){
            modalData.RoleId = modalData.Role.id;
            var getRoleFunction = companyService.getRoleFunction(modalData);
            getRoleFunction.then(function(respone){
                $scope.FunctionList = dictionaryService.FunctionPermissionCategory();
                $scope.FunctionListId = respone.functions;
                angular.forEach($scope.FunctionList,function(val,key){
                    val.checked = false;
                    val.iChecked = false;
                });
                angular.forEach(respone.functions,function(val,key){
                    $scope.FunctionList[val.item].checked = true;
                    $scope.FunctionList[val.item].iChecked = true;
                });
            },_error);
        }
        function _setRoleFunction(){
            var getRoleFunction = companyService.setRoleFunction(modalData);
            getRoleFunction.then(function(respone){
                _success('编辑成功');
                $timeout(function () {
                    $uibModalInstance.close();
                }, 2000);
            },_error);
        }
        function _isChecked(){
            if(!$scope.RoleInfo.type) {
                this.Function.iChecked = !this.Function.iChecked;
            }else{
                this.Function.checked = this.Function.iChecked;
            }
        }
        function _success(msg){
            globalConfig.rootScope.successMessage = msg;
            globalConfig.rootScope.errorMessage = '';
        }
        function _error(err){
            if(err) {
                globalConfig.rootScope.successMessage = '';
                globalConfig.rootScope.errorMessage = err.message.join('&lt;br&frasl;&gt;');
            }
        }
        $scope.ok = function () {
            modalData.Function = [];
            angular.forEach($scope.FunctionList,function(val,key){
                if(val.checked) {
                    modalData.Function.push(val.Value);
                }
            });
            _setRoleFunction();
        };
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }


    function control(globalConfig,$scope,$state,companyService,$timeout,$modal,currentUserService){
        var currentUser;
        var items = {};
        /*操作*/
        var handle1 = {"name":"删除选择角色","id":1};
        globalConfig.rootScope.app.title = '角色列表 - 点滴科技经纪云办公平台';

        $scope.handle = [];
        $scope.roleList;
        $scope.openAddRoleModal = _openAddRoleModal;
        $scope.openExitRoleModal = _openExitRoleModal;
        $scope.openDelectRoleModal = _openDelectRoleModal;
        $scope.openShowFunctionRangeModal = _openShowFunctionRangeModal;
        $scope.toRoleMemberList = _toRoleMemberList;
        $scope.isCheck = _isCheck;
        $scope.checkedAll = _checkedAll;
        $scope.openControl = _openControl;

        /*初始化*/
        init();

        function init() {
            currentUserService.currentUser().then(function(data){
                currentUser = data;
                $scope.pageSize = globalConfig.rootScope.paramMemberCondition.PageSize;
                $scope.Page = globalConfig.rootScope.paramMemberCondition.CurrentPage;
                _getRoleList(currentUser.company.id);
                //$scope.handle.push(handle1);
            },_error);
        }

        /*获取角色
         * @param cid 公司id
         * */
        function _getRoleList(cid){
            var getRoleList = companyService.getRoleList(cid);
            getRoleList.then(function(data){
                $scope.roleList = _judgeChecked(data.list);
            },_error);
        }

        /*判断是否已选择*/
        function _judgeChecked(list){
            angular.forEach(list,function(val1,key1){
                val1.checked = false;
                angular.forEach(items,function(val2,key2){
                    if(val1.id==val2){
                        val1.checked = true;
                    }
                });
            });
            return list;
        }
        /*选择成员*/
        function _isCheck(){
            var key = this.i.id;
            if(this.i.checked){
                items[key] = this.i.id;
            }else{
                delete items[key];
            }
        }
        function _checkedAll(){
            var checkAllMember = $scope.checkAll;
            if(checkAllMember) {
                angular.forEach($scope.roleList, function (val, key) {
                    if(!val.checked){
                        val.checked = !val.checked;
                        items[val.id] = val.id;
                    }
                });
            }else{
                angular.forEach($scope.roleList, function (val, key) {
                    if(val.checked){
                        val.checked = !val.checked;
                        delete items[val.id];
                    }
                });
            }
        }

        /*打开添加角色弹出框*/
        function _openAddRoleModal (size) {
            var modalInstance = $modal.open({
                animation: true,
                templateUrl: 'modalAddRole.html',
                controller: 'ModalAddRoleCtrl',
                size: size,
                backdrop:'static',
                resolve: {
                    modalData:{
                        title:'添加角色'
                    }
                }
            });

            modalInstance.result.then(function (form) {
                _getRoleList(currentUser.company.id);
            }, _error);
        }
        /*打开编辑角色弹出框*/
        function _openExitRoleModal (size) {
            var modalInstance = $modal.open({
                animation: true,
                templateUrl: 'modalAddRole.html',
                controller: 'ModalAddRoleCtrl',
                size: size,
                backdrop:'static',
                resolve: {
                    modalData:{
                        title:'编辑角色',
                        role:this.i
                    }
                }
            });

            modalInstance.result.then(function (form) {
                _getRoleList(currentUser.company.id);
            }, _error);
        }
        /*打开删除角色弹出框
         * */
        function _openDelectRoleModal(size){
            var modal_url = "modalToDelect.html";
            var modal_ctl = "ModalDelectRoleCtrl";
            var modalInstance = $modal.open({
                animation: true,
                templateUrl: modal_url,
                controller: modal_ctl,
                size: size,
                backdrop:'static',
                resolve: {
                    modalData:{
                        controller:{
                            name:'删除角色',
                            message:'删除角色后，成员将全部从该角色移出。'
                        },
                        role:this.i
                    }
                }
            });
            modalInstance.result.then(function (form) {
                _getRoleList(currentUser.company.id);
            }, _error);
        }

        /*打开批量操作弹出框*/
        function _openControl (size) {
            if($scope.controller!=null) {
                var modal_url,modal_ctl;
                var modal_data = {};
                if($scope.controller.id==1) {
                    /*执行id1 handle*/
                }
            }
        }

        /*跳转人员管理*/
        function _toRoleMemberList(){
            $state.go('admin.role.roleMemberList',{RoleId:this.i.id});
        }
        /*打开浏览权限弹出框
         * */
        function _openShowFunctionRangeModal(size){
            var vm = this.i;
            vm.RoleId = vm.id;
            var modal_data = {
                controller:{
                    name:''
                },
                Role:vm,
                CompanyId:currentUser.company.id
            };
            var modal_url = "modalSetRoleFunction.html";
            var modal_ctl = "ModalSetRoleFunctionCtrl";
            modal_data.controller.name = '功能权限';
            var modalInstance = $modal.open({
                animation: true,
                templateUrl: modal_url,
                controller: modal_ctl,
                size: size,
                backdrop:'static',
                resolve: {
                    modalData: function () {
                        return modal_data;
                    }
                }
            });
        }


        function _success(msg){
            globalConfig.rootScope.successMessage = msg;
            globalConfig.rootScope.errorMessage = '';
        }
        function _error(err){
            if(err) {
                globalConfig.rootScope.successMessage = '';
                globalConfig.rootScope.errorMessage = err.message.join('&lt;br&frasl;&gt;');
            }
        }
    }
});