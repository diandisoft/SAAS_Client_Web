/**
 * Created by jianbo on 2016/1/30.
 * 成员个人资料
 */
'use strict';

define(['app',"../companyService"],function (app) {
    app.controller('MemberInfoController',control);
    control.$inject = ['globalConfig','$scope','cryptoJS','companyService','$validation','$stateParams','currentUserService','dictionaryService'];
    function control(globalConfig,$scope,cryptoJS,companyService,$validationProvider,$stateParams,currentUserService,dictionaryService) {

        var currentUser;
        init();
        function init() {
            currentUserService.currentUser().then(function (response) {
                currentUser = response;
                _init();
            }, _error);
        }

        function _init(){
            var data = {};
            data.CompanyId = currentUser.company.id;
            data.MemberId = $stateParams.data;
            var exitMember = companyService.paramMember(data);
            exitMember.then(
                function(data){
                    $scope.user = data.memberinfo;
                    $scope.user.gender = dictionaryService.Gender()[$scope.user.gender].Description;
                    $scope.user.createTime = $scope.user.createTime.toDate();

                    if($scope.user.birthday !=undefined){
                        $scope.user.birthday = $scope.user.birthday.toDate();
                    }
                    var status = $scope.user.status;
                    if ($scope.user.roleList.length <= 0) {
                        $scope.user.roleList = {'name': '未分配'}
                    }
                    switch (status) {
                        case 0:
                            $scope.user.status = "未激活";
                            break;
                        case 1:
                            $scope.user.status = "未审核";
                            break;
                        case 2:
                            $scope.user.status = "正常";
                            break;
                    }
                },function(err){
                    globalConfig.rootScope.errorMessage = err.message.join('&lt;br&frasl;&gt;');
                }
            );
        }

        function _error(err){
            if (err) {
                globalConfig.rootScope.errorMessage = err.message.join('&lt;br&frasl;&gt;');
            }
        }

    }
});