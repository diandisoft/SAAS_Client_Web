/**
 * Created by jianbo on 2016/1/19.
 * 公司管理
 */

'use strict';

define(['app',"../companyService"],function (app) {

    /*批量员工操作弹出款Ctl*/
    app.controller('ModalSetDimissionCtrl',controlSetDimission);
    controlSetDimission.$inject = ['globalConfig','$scope','$uibModalInstance','modalData','$validation','currentUserService','$modal','companyService'];
    app.controller('ModalComfirmMemberCtrl',controlComfirmMember);
    controlComfirmMember.$inject = ['globalConfig','$scope','$uibModalInstance','modalData','$validation','currentUserService','$timeout','companyService'];
    app.controller('ModalTransferDepartmentCtrl',controlTransferDepartment);
    controlTransferDepartment.$inject = ['globalConfig','$scope','$uibModalInstance','modalData','currentUserService','companyService','$timeout'];
    /*单个员工操作弹出款Ctl*/
    app.controller('ModalExitMemberCtrl',controlExitMember);
    controlExitMember.$inject = ['globalConfig','$scope','$uibModalInstance','modalData','currentUserService','companyService','$validation','$modal','$timeout'];
    app.controller('ModalAddMemberCtrl',controlAddMember);
    controlAddMember.$inject = ['globalConfig','$scope','$uibModalInstance','modalData','currentUserService','companyService','$validation','$modal','$timeout'];
    app.controller('ModalDeleteMemberCtrl',controlDeleteMember);
    controlDeleteMember.$inject = ['globalConfig','$scope','$uibModalInstance','modalData','currentUserService','companyService','$validation','$modal','$timeout'];
    /*选择其他成员*/
    app.controller('ModalSelectOtherMemberCtrl',controlSelectOtherMember);
    controlSelectOtherMember.$inject = ['globalConfig','$scope','$uibModalInstance','modalData','currentUserService','companyService','$timeout'];
    /*部门操作弹出款Ctl*/
    app.controller('ModalExitDepartmentCtrl',controlExitDepartment);
    controlExitDepartment.$inject = ['globalConfig','$scope','$uibModalInstance','modalData','$validation','companyService','currentUserService','$timeout','$modal'];
    /*部门操作-添加部门弹出款Ctl*/
    app.controller('ModalExitDepartmentToAddCtrl',controlExitDepartmentToAdd);
    controlExitDepartmentToAdd.$inject = ['globalConfig','$scope','$uibModalInstance','modalData','currentUserService','companyService','$validation','$modal','$timeout'];
    /*部门操作-删除部门弹出款Ctl*/
    app.controller('ModalToDelectCtrl',controlToDelect);
    controlToDelect.$inject = ['globalConfig','$scope','$uibModalInstance','modalData','$validation','currentUserService','companyService','$timeout'];

    /*批量导入人员弹出款Ctl*/
    app.controller('ModalCvsCtrl',controlCvs);
    controlCvs.$inject = ['globalConfig','$scope','$uibModalInstance','modalData','$validation','companyService','currentUserService'];
    /*导出人员列表弹出款Ctl*/
    app.controller('ModalExportCvsCtrl',controlExportCvs);
    controlExportCvs.$inject = ['globalConfig','$scope','$uibModalInstance','modalData','$validation','companyService','currentUserService'];

    app.controller('DepartmentController',control);
    control.$inject = ['globalConfig','$scope','$state','companyService','$timeout','$modal','currentUserService','dictionaryService'];

    function controlSetDimission(globalConfig,$scope,$uibModalInstance,modalData,$validationProvider,currentUserService,$modal,companyService) {
        $scope.title = modalData.controller.name;
        $scope.checkValid = $validationProvider.checkValid;
        $scope.selectMember = _selectMember;
        $scope.form = {
            people:'',
            code:''
        };
        function _selectMember(){
            var modal_data = {
                controller:{
                    name:''
                }
            };
            var modal_url = "modalSelectOtherMember.html";
            var modal_ctl = "ModalSelectOtherMemberCtrl";
            modal_data.controller.name = '选择转授人';
            var size = 'lg';
            var modalInstance = $modal.open({
                animation: true,
                templateUrl: modal_url,
                controller: modal_ctl,
                size: size,
                backdrop:'static',
                resolve: {
                    modalData: function () {
                        return modal_data;
                    }
                }
            });
            modalInstance.result.then(function (data) {
                $scope.form.people = data.i;
            }, _error);
        }
        $scope.ok = function (form) {
            $uibModalInstance.close(form);
        };
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
        function _error(err){
            if(err) {
                globalConfig.rootScope.successMessage = '';
                globalConfig.rootScope.errorMessage = err.message.join('&lt;br&frasl;&gt;');
            }
        }
    }
    function controlComfirmMember(globalConfig,$scope,$uibModalInstance,modalData,$validationProvider,currentUserService,$timeout,companyService) {
        $scope.title = modalData.controller.name;
        $scope.roles = modalData.roles.list;
        $scope.form = {
            roles:[],
            code:'',
            roleList:{}
        };
        $scope.checkValid = $validationProvider.checkValid;
        $scope.ok = function (form) {
            angular.forEach($scope.form.roleList,function(value,key){
                $scope.form.roles.push(value.id);
            });
            angular.forEach(modalData.menberId,function(value,key){
                _comfirmMember(value,true,$scope.form.roles);
            });
        };
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
        function _comfirmMember(mid,isComfirm,roles){
            currentUserService.currentUser().then(function(currentUser) {
                var data = {};
                data.CompanyId = currentUser.company.id;
                data.MemberId = mid;
                data.Confirmed = isComfirm;
                var comfirmMember = companyService.comfirmMember(data);
                comfirmMember.then(function () {
                    if(roles.length>0) {
                        data.RoleId = roles;
                        _setRole(data);
                    }else{
                        _success('操作成功');
                        $timeout(function () {
                            $uibModalInstance.close();
                        }, 2000);
                    }
                }, _error);
            }, _error);
        }
        function _setRole(data){
            var exitRole = companyService.exitMemberRole(data);
            exitRole.then(function(){
                _success('操作成功');
                $timeout(function () {
                    $uibModalInstance.close();
                }, 2000);
            },_error);
        }
        function _success(msg){
            globalConfig.rootScope.successMessage = msg;
            globalConfig.rootScope.errorMessage = '';
        }
        function _error(err){
            if(err) {
                globalConfig.rootScope.successMessage = '';
                globalConfig.rootScope.errorMessage = err.message.join('&lt;br&frasl;&gt;');
            }
        }
    }
    function controlTransferDepartment(globalConfig,$scope, $uibModalInstance,modalData,currentUserService,companyService,$timeout) {
        var tree,currentUser;
        $scope.doing_async1 = true;
        $scope.depList=[];
        $scope.my_tree1 = tree = {};
        $scope.title = modalData.controller.name;
        currentUserService.currentUser().then(function(data) {
            currentUser = data;
            var list = companyService.getDepartment(currentUser.company.id);
            list.then(
                function (data) {
                    $scope.depList = data.tree;
                    $scope.doing_async1 = false;
                    return $timeout(
                        function() {
                            tree.select_first_branch();
                            tree.expand_all();
                        },300);
                }
            );
        });

        $scope.ok = function () {
            $uibModalInstance.close(tree.get_selected_branch());
        };
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
        function _error(err){
            if(err) {
                globalConfig.rootScope.successMessage = '';
                globalConfig.rootScope.errorMessage = err.message.join('&lt;br&frasl;&gt;');
            }
        }
    }

    function controlSelectOtherMember(globalConfig,$scope, $uibModalInstance,modalData,currentUserService,companyService,$timeout) {
        var tree,Department,currentUser,member;
        /*成员列表初始条件*/
        var paramMemberCondition = {
            CompanyId:" ",
            Name:" ",
            Phone:" ",
            Position:" ",
            RoleId:" ",
            DepartmentId:" ",
            Status:"2",
            IsDimission:"false",
            SortOrder:"0",
            CurrentPage:1,
            PageSize:10
        };
        $scope.numPages = 5;//分页显示数量
        $scope.pageSize = paramMemberCondition.PageSize;
        $scope.doing_async1 = true;
        $scope.depList=[];
        $scope.my_tree1 = tree = {};
        $scope.title = modalData.controller.name;
        $scope.myTreeHandler1 = _myTreeHandler1;
        $scope.changePageHandler = _changePageHandler;
        $scope.selectMember = _selectMember;
        $scope.byNameHandler1 = _byNameHandler1;
        $scope.byRoleHandler1 = _byRoleHandler1;
        init();

        function init() {
            currentUserService.currentUser().then(function(data){
                currentUser = data;
                paramMemberCondition.CompanyId = currentUser.company.id;
                paramMemberCondition.Position = " ";
                _getRolt(currentUser.company.id);
                _getDepartmentList();
            },_error);
        }

        function _getDepartmentList(){
            var list = companyService.getDepartment(currentUser.company.id);
            list.then(
                function (data) {
                    $scope.depList = data.tree;
                    $scope.doing_async1 = false;
                    return $timeout(
                        function() {
                            tree.select_first_branch();
                            tree.expand_all();
                        },300);
                }
            );
        }

        function _myTreeHandler1 (branch) {
            paramMemberCondition.DepartmentId = branch.depId;
            paramMemberCondition.IsDimission = 'false';
            Department = branch;
            $scope.depName = branch.label;
            paramMemberCondition.CurrentPage = 1;
            _getMemberList();
        }

        /*姓名\电话\职位搜索*/
        function _byNameHandler1(vm) {
            if(vm === undefined||vm.people_name === undefined||vm.people_name==''){
                paramMemberCondition.Name=" ";
            }else {
                paramMemberCondition.Name = vm.people_name.replace(/[ ]/g,"");
            }
            if(vm === undefined||vm.people_phone === undefined||vm.people_phone==''){
                paramMemberCondition.Phone=" ";
            }else {
                paramMemberCondition.Phone = vm.people_phone.replace(/[ ]/g,"");
            }
            if(vm === undefined||vm.people_position === undefined||vm.people_position==''){
                paramMemberCondition.Position=" ";
            }else {
                paramMemberCondition.Position = vm.people_position.replace(/[ ]/g,"");
            }
            console.info(vm,paramMemberCondition);
            paramMemberCondition.CurrentPage = 1;
            _getMemberList();
        };
        /*角色搜索*/
        function _byRoleHandler1(selected){
            if(selected!=null) {
                paramMemberCondition.RoleId = selected.id;
            }else{
                paramMemberCondition.RoleId = " ";
            }
            paramMemberCondition.CurrentPage = 1;
            _getMemberList();
        }
        function _getRolt(cid) {
            var getRoltList = companyService.getRoleList(cid);
            getRoltList.then(function (data) {
                $scope.roles = data.list;
            }, _error);
        }
        /*获取成员列表*/
        function _getMemberList(){
            var memberList = companyService.getCompanyMemberList(paramMemberCondition);
            memberList.then(function(data){
                $scope.MemberList = data.list;
                $scope.bigTotalItems = data.listcount;
            },_error);
        }
        /*翻页*/
        function _changePageHandler() {
            paramMemberCondition.CurrentPage = this.P;
            _getMemberList();
        }
        /*选择成员*/
        function _selectMember(){
            $scope.selected = this;
        }
        function _error(err){
            if(err) {
                globalConfig.rootScope.successMessage = '';
                globalConfig.rootScope.errorMessage = err.message.join('&lt;br&frasl;&gt;');
            }
        }
        $scope.ok = function () {
            $uibModalInstance.close($scope.selected);
        };
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }

    function controlExitMember(globalConfig,$scope, $uibModalInstance,modalData,currentUserService,companyService,$validationProvider,$modal,$timeout) {
        var data = {};
        $scope.popup = {
            opened: false
        };
        $scope.checkValid = $validationProvider.checkValid;
        $scope.selectDepartment = _selectDepartment;
        $scope.openDate = _openDate;
        $scope.today =  new Date();
        init();
        function init() {
            currentUserService.currentUser().then(function (currentUser) {
                $scope.departmentList = {};
                data.CompanyId = currentUser.company.id;
                data.MemberId = modalData.MemberId;
                var paramMember = companyService.paramMember(data);
                paramMember.then(
                    function (data) {
                        $scope.user = data.memberinfo;
                        if($scope.user.birthday !=undefined){
                            $scope.user.birthday = $scope.user.birthday.toDate();
                        }
                        data.OldDepartmentId = $scope.user.depId;
                        data.RoleId = [];
                        data.CompanyId = currentUser.company.id;
                        _getRolt(data.CompanyId);
                    },_error);
            }, _error);
        }
        /*打开选择日期*/
        function _openDate($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.popup.opened = true;
        }
        function _selectDepartment(){
            var modal_data = {
                controller:{
                    name:''
                }
            };
            var modal_url = "modalTransferDepartment.html";
            var modal_ctl = "ModalTransferDepartmentCtrl";
            modal_data.controller.name = '选择门店';
            var size = 'sm';
            var modalInstance = $modal.open({
                animation: true,
                templateUrl: modal_url,
                controller: modal_ctl,
                size: size,
                backdrop:'static',
                resolve: {
                    modalData: function () {
                        return modal_data;
                    }
                }
            });
            modalInstance.result.then(function (data) {
                $scope.user.departmentList[0].id = data.depId;
                $scope.user.departmentList[0].name = data.label;
            }, _error);
        }

        /*分配角色*/
        function _setRole(user){
            var exitMemberRole = companyService.exitMemberRole(user);
            exitMemberRole.then(function(data){
                _success('操作成功');
                $timeout(function () {
                    $uibModalInstance.close();
                }, 2000);
            },_error);
        }
        /*获取角色
         * @param cid 公司id
         * */
        function _getRolt(cid) {
            var getRoltList = companyService.getRoleList(cid);
            getRoltList.then(function (data) {
                $scope.roles = data.list;
            }, _error);
        }

        $scope.ok = function (user) {
            var roles = [];
            user.CompanyId = data.CompanyId;
            user.MemberId = user.id;
            var exitMember = companyService.exitMember(user);
            angular.forEach($scope.user.roleList,function(value,key){
                roles.push(value.id);
            });
            exitMember.then(
                function(){
                    user.DepartmentId = [];
                    user.DepartmentId.push($scope.user.departmentList[0].id);
                    var transferDepartmentForOne = companyService.transferDepartmentForOne(user);
                    transferDepartmentForOne.then(function(){
                        if(roles.length>0) {
                            user.RoleId = roles;
                            _setRole(user);
                        }else{
                            _success('操作成功');
                            $timeout(function () {
                                $uibModalInstance.close();
                            }, 2000);
                        }
                    },_error);
                },_error);
        };
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
        function _success(msg){
            globalConfig.rootScope.successMessage = msg;
            globalConfig.rootScope.errorMessage = '';
        }
        function _error(err){
            if(err) {
                globalConfig.rootScope.successMessage = '';
                globalConfig.rootScope.errorMessage = err.message.join('&lt;br&frasl;&gt;');
            }
        }
    }
    function controlAddMember(globalConfig,$scope, $uibModalInstance,modalData,currentUserService,companyService,$validationProvider,$modal,$timeout) {
        var data = {};
        $scope.selectDepartment = _selectDepartment;
        $scope.md5 = function(){
            $scope.vm['Password'] = cryptoJS.MD5($scope.pwd).toString();
        }
        $scope.checkValid = $validationProvider.checkValid;

        /*获取部门*/
        function _selectDepartment(){
            var modal_data = {
                controller:{
                    name:''
                }
            };
            var modal_url = "modalTransferDepartment.html";
            var modal_ctl = "ModalTransferDepartmentCtrl";
            modal_data.controller.name = '选择门店';
            var size = 'sm';
            var modalInstance = $modal.open({
                animation: true,
                templateUrl: modal_url,
                controller: modal_ctl,
                size: size,
                backdrop:'static',
                resolve: {
                    modalData: function () {
                        return modal_data;
                    }
                }
            });
            modalInstance.result.then(function (data) {
                    $scope.Department = data;
            }, _error);
        }
        function _addMemberSuccess(response){
            $uibModalInstance.close();
            data.MemberId = response.memberid;
            var sendSMS = _sendSMS();
            sendSMS.then(function () {
                _success('添加成员成功，正在发送邀请短信...');
            },_error);
        }

        $scope.ok = function (user) {
            currentUserService.currentUser().then(function (currentUser) {
                user.CompanyId = currentUser.company.id;
                user.DepartmentId = $scope.Department.depId;

                companyService.addMember(user).then(function(response){
                    data.CompanyId = currentUser.company.id
                    data.MemberId = response.memberid;
                    companyService.sendSMS(data).then(function () {
                        _success('添加成员成功，正在发送邀请短信...');
                        $timeout(function () {
                            $uibModalInstance.close();
                        }, 2000);
                    },_error);
                }, _error);
            });
        };
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

        function _success(msg){
            globalConfig.rootScope.successMessage = msg;
            globalConfig.rootScope.errorMessage = '';
        }
        function _error(err){
            if(err) {
                globalConfig.rootScope.successMessage = '';
                globalConfig.rootScope.errorMessage = err.message.join('&lt;br&frasl;&gt;');
            }
        }
    }
    function controlDeleteMember(globalConfig,$scope, $uibModalInstance,modalData,currentUserService,companyService,$validationProvider,$modal,$timeout) {
        $scope.checkValid = $validationProvider.checkValid;
        $scope.selectMember = _selectMember;
        $scope.form = {
            people:'',
            code:''
        };
        function _selectMember(){
            var modal_data = {
                controller:{
                    name:''
                }
            };
            var modal_url = "modalSelectOtherMember.html";
            var modal_ctl = "ModalSelectOtherMemberCtrl";
            modal_data.controller.name = '选择转授人';
            var size = 'lg';
            var modalInstance = $modal.open({
                animation: true,
                templateUrl: modal_url,
                controller: modal_ctl,
                size: size,
                backdrop:'static',
                resolve: {
                    modalData: function () {
                        return modal_data;
                    }
                }
            });
            modalInstance.result.then(function (data) {
                $scope.form.people = data.i;
            }, _error);
        }
        $scope.ok = function (form) {
            currentUserService.currentUser().then(function(currentUser) {
                var vm = {};
                vm.CompanyId = currentUser.company.id;
                vm.MemberId = modalData.MemberId;
                vm.Code = form.code;
                vm.TransferId = form.people.id;
                var deleteMember = companyService.deleteMember(vm);
                deleteMember.then(function () {
                    _success('删除成功');
                    $timeout(function () {
                        $uibModalInstance.close();
                    }, 2000);
                },_error);
            },_error);
        };
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
        function _success(msg){
            globalConfig.rootScope.successMessage = msg;
            globalConfig.rootScope.errorMessage = '';
        }
        function _error(err){
            if(err) {
                globalConfig.rootScope.successMessage = '';
                globalConfig.rootScope.errorMessage = err.message.join('&lt;br&frasl;&gt;');
            }
        }
    }
    function controlExitDepartment(globalConfig,$scope, $uibModalInstance,modalData,$validationProvider,companyService,currentUserService,$timeout,$modal) {
        var tree,currentUser;
        $scope.doing_async1 = true;
        $scope.depList=[];
        $scope.my_tree1 = tree = {};
        $scope.myTreeHandler2 = _myTreeHandler2;
        $scope.addDepartment = _addDepartment;
        $scope.exitDepartment = _exitDepartment;
        $scope.delectDepartment = _delectDepartment;
        init();
        function init() {
            currentUserService.currentUser().then(function (data) {
                currentUser = data;
                var list = companyService.getDepartment(currentUser.company.id);
                list.then(
                    function (data) {
                        $scope.depList = data.tree;
                        $scope.doing_async1 = false;
                        return $timeout(
                            function () {
                                tree.select_first_branch();
                                tree.expand_all();
                            }, 300);
                    }
                );
            });
        }
        /*选中门店*/
        function _myTreeHandler2(branch){
            if(branch.parentId==undefined){
                $scope.btn = true;
            }else{
                $scope.btn = false;
            }
        }
        /*新增门店*/
        function _addDepartment(size){
            var modal_data = {
                controller:{
                    name:''
                }
            };
            var modal_url = "modalExitDepartmentToAdd.html";
            var modal_ctl = "ModalExitDepartmentToAddCtrl";
            modal_data.controller.name = '添加门店';
            var modalInstance = $modal.open({
                animation: true,
                templateUrl: modal_url,
                controller: modal_ctl,
                size: size,
                backdrop:'static',
                resolve: {
                    modalData: function () {
                        return modal_data;
                    }
                }
            });
            modalInstance.result.then(function (vm) {
                init();
            }, _error);
        }
        /*编辑门店*/
        function _exitDepartment(size){
            var modal_data = {
                controller:{
                    name:''
                }
            };
            var modal_url = "modalExitDepartmentToAdd.html";
            var modal_ctl = "ModalExitDepartmentToAddCtrl";
            modal_data.controller.name = '编辑门店';
            modal_data.controller.dep = tree.get_selected_branch();
            var modalInstance = $modal.open({
                animation: true,
                templateUrl: modal_url,
                controller: modal_ctl,
                size: size,
                backdrop:'static',
                resolve: {
                    modalData: function () {
                        return modal_data;
                    }
                }
            });
            modalInstance.result.then(function (vm) {
                init();
            }, _error);
        }
        /*删除门店*/
        function _delectDepartment(size){
            var modal_data = {
                controller:{
                    name:''
                }
            };
            var modal_url = "modalToDelect.html";
            var modal_ctl = "ModalToDelectCtrl";
            modal_data.controller.name = '删除门店';
            modal_data.controller.message = '删除部门后，部门成员将全部放入上一层级联系人中。';
            modal_data.controller.dep = tree.get_selected_branch();
            var modalInstance = $modal.open({
                animation: true,
                templateUrl: modal_url,
                controller: modal_ctl,
                size: size,
                backdrop:'static',
                resolve: {
                    modalData: function () {
                        return modal_data;
                    }
                }
            });
            modalInstance.result.then(function (vm) {
                init();
            }, _error);
        }

        $scope.ok = function () {
            $uibModalInstance.close(tree.get_selected_branch());
        };
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
        function _success(msg){
                globalConfig.rootScope.successMessage = msg;
                globalConfig.rootScope.errorMessage = '';
        }
        function _error(err){
            if(err) {
                globalConfig.rootScope.successMessage = '';
                globalConfig.rootScope.errorMessage = err.message.join('&lt;br&frasl;&gt;');
            }
        }
    }
    function controlExitDepartmentToAdd(globalConfig,$scope, $uibModalInstance,modalData,currentUserService,companyService,$validationProvider,$modal,$timeout) {
        var currentUser;
        var data = {};
        $scope.Department = {};
        $scope.vm={
            Caption:"",
            Tel:"",
            CityId:"",
            ParentDepartmentId:""
        }
        $scope.title = modalData.controller.name;
        $scope.selectDepartment = _selectDepartment;
        $scope.checkValid = $validationProvider.checkValid;

        init();
        function init() {
            currentUserService.currentUser().then(function (response) {
                currentUser = response;
                data.CompanyId = currentUser.company.id;
                if(modalData.controller.dep!=undefined){
                    data.DepartmentId = modalData.controller.dep.depId;
                    var getDepartmentIdInfo = companyService.getDepartmentInfo(data);
                    getDepartmentIdInfo.then(function(response1){
                        data.DepartmentId = response1.departmentvm.parentDepartmentId;
                        var getParentDepartmentInfo = companyService.getDepartmentInfo(data);
                        getParentDepartmentInfo.then(function(response2){
                            $scope.Department.label = response2.departmentvm.caption;
                            $scope.vm={
                                Caption:response1.departmentvm.caption,
                                Tel:response1.departmentvm.tel,
                                CityId:response1.departmentvm.cityId,
                                ParentDepartmentId:response1.departmentvm.parentDepartmentId
                            }
                        });
                    },_error);
                }
                $scope.cityList = [{cityId:1,label:"珠海"},{cityId:2,label:"中山"},{cityId:3,label:"斗门"}];
            }, _error);
        }
        /*获取部门*/
        function _selectDepartment(){
            var modal_data = {
                controller:{
                    name:''
                }
            };
            var modal_url = "modalTransferDepartment.html";
            var modal_ctl = "ModalTransferDepartmentCtrl";
            modal_data.controller.name = '选择门店';
            var size = 'sm';
            var modalInstance = $modal.open({
                animation: true,
                templateUrl: modal_url,
                controller: modal_ctl,
                size: size,
                backdrop:'static',
                resolve: {
                    modalData: function () {
                        return modal_data;
                    }
                }
            });
            modalInstance.result.then(function (data) {
                $scope.Department = data;
                $scope.vm.ParentDepartmentId = data.depId;
            }, _error);
        }

        $scope.ok = function (vm) {
            vm.CompanyId = currentUser.company.id;
            var Department;
            if(modalData.controller.dep === undefined) {
                Department = companyService.addDepartment(vm);
            }else{
                vm.DepartmentId = modalData.controller.dep.depId;
                Department = companyService.exitDepartment(vm);
            }
            Department.then(function() {
                _success('操作成功');
                $timeout(function() {
                    $uibModalInstance.close();
                }, 2000);
            }, _error);
        };
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
        function _success(msg){
            globalConfig.rootScope.successMessage = msg;
            globalConfig.rootScope.errorMessage = '';
        }
        function _error(err){
            if(err) {
                globalConfig.rootScope.successMessage = '';
                globalConfig.rootScope.errorMessage = err.message.join('&lt;br&frasl;&gt;');
            }
        }
    }
    function controlToDelect(globalConfig,$scope,$uibModalInstance,modalData,$validationProvider,currentUserService,companyService,$timeout) {
        $scope.title = modalData.controller.name;
        $scope.message = modalData.controller.message;
        $scope.checkValid = $validationProvider.checkValid;
        $scope.ok = function (form) {
            currentUserService.currentUser().then(function (currentUser) {
                form.CompanyId = currentUser.company.id;
                form.DepartmentId = modalData.controller.dep.depId;
                var exitDepartment = companyService.deleteDepartment(form);
                exitDepartment.then(
                    function(data) {
                        _success('操作成功');
                        $timeout(function() {
                            $uibModalInstance.close();
                        }, 2000);
                    },_error
                );
            });
        };
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
        function _success(msg){
            globalConfig.rootScope.successMessage = msg;
            globalConfig.rootScope.errorMessage = '';
        }
        function _error(err){
            if(err) {
                globalConfig.rootScope.successMessage = '';
                globalConfig.rootScope.errorMessage = err.message.join('&lt;br&frasl;&gt;');
            }
        }
    }
    function controlCvs(globalConfig,$scope, $uibModalInstance,modalData,$validationProvider,companyService,currentUserService) {
        var currentUser,data;
        $scope.checkValid = $validationProvider.checkValid;
        $scope.downloadCvs = _downloadCvs;
        init();
        function init() {
            currentUserService.currentUser().then(function (response) {
                currentUser = response;
                data.CompanyId = currentUser.company.id;
            }, _error);
        }
        function _downloadCvs(){
            companyService.downloadCvs(data).then(function(){
                _success('开始下载');
            },_error);
        }

        $scope.ok = function (form) {
            $uibModalInstance.close(form);
        };
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
        function _success(msg){
            globalConfig.rootScope.successMessage = msg;
            globalConfig.rootScope.errorMessage = '';
        }
        function _error(err){
            if(err) {
                globalConfig.rootScope.successMessage = '';
                globalConfig.rootScope.errorMessage = err.message.join('&lt;br&frasl;&gt;');
            }
        }
    }
    function controlExportCvs(globalConfig,$scope, $uibModalInstance,modalData,$validationProvider,companyService,currentUserService) {
        var currentUser,data;
        $scope.checkValid = $validationProvider.checkValid;
        init();
        function init() {
            currentUserService.currentUser().then(function (response) {
                currentUser = response;
                data.CompanyId = currentUser.company.id;
            }, _error);
        }

        $scope.ok = function (form) {
            $uibModalInstance.close(form);
        };
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
        function _error(err){
            if(err) {
                globalConfig.rootScope.successMessage = '';
                globalConfig.rootScope.errorMessage = err.message.join('&lt;br&frasl;&gt;');
            }
        }
    }

    function control(globalConfig,$scope,$state,companyService,$timeout,$modal,currentUserService,dictionaryService){
        /*初始化*/
        init();
        var tree;
        var items = {};
        var Department,currentUser;
        globalConfig.rootScope.app.title = '门店列表 - 点滴科技经纪云办公平台';

        $scope.my_tree = tree = {};
        $scope.groups = [];
        $scope.handle = [];
        $scope.doing_async = true;
        $scope.numPages = 5;//分页显示数量
        $scope.pageSize = globalConfig.rootScope.paramMemberCondition.PageSize;
        $scope.bigTotalItems = 0;//item总数
        $scope.role = "";
        $scope.people_name='';
        $scope.people_phone='';
        $scope.people_position='';

        $scope.myTreeHandler = _myTreeHandler;
        $scope.byRoleHandler = _byRoleHandler;
        $scope.byNameHandler = _byNameHandler;
        $scope.changePageHandler = _changePageHandler;
        $scope.sortOrderHandler =  _sortOrderHandler;
        $scope.isCheck = _isCheck;
        $scope.checkedAll = _checkedAll;
        $scope.openControl = _openControl;
        $scope.openForExitDepartment = _openForExitDepartment;
        $scope.openForexitMember = _openForexitMember;
        $scope.openForaddMember = _openForaddMember;
        $scope.openForDeleteMember = _openForDeleteMember;
        $scope.openMember = _openMember;
        $scope.openForCvs = _openForCvs;
        $scope.openForExportCvs = _openForExportCvs;
        $scope.openForcomfirmMember = _openForcomfirmMember;
        $scope.sendSMS = _sendSMS;
        $scope.setArchives = _setArchives;
        $scope.openMemberInfo = _openMemberInfo;

        /*操作*/
        var handle1 = {"name":"选中人员设为离职","id":1};
        var handle2 = {"name":"选中人员通过审核","id":2};
        var handle3 = {"name":"选中人员转移部门","id":3};



        function init() {
            currentUserService.currentUser().then(function(data){
                currentUser = data;
                globalConfig.rootScope.paramMemberCondition.CompanyId = currentUser.company.id;
                globalConfig.rootScope.paramMemberCondition.Status = " ";
                globalConfig.rootScope.paramMemberCondition.Position = " ";
                _getDepartmentList();
                _getRoleList(globalConfig.rootScope.paramMemberCondition.CompanyId);
                $scope.handle.push(handle1);
                $scope.handle.push(handle3);
            },_error);

        }
        function _openMember(){
            var member = $('#member');
            member.addClass('show');
            var data = {};
            data.CompanyId = currentUser.company.id;
            data.MemberId = this.i.id;
            var exitMember = companyService.paramMember(data);
            exitMember.then(function(data){
                $scope.user = data.memberinfo;
                $scope.user.gender = dictionaryService.Gender()[$scope.user.gender].Description;
                var status = $scope.user.status;
                console.info(status);
                switch (status) {
                    case 0:
                        $scope.user.status = "未激活";
                        $scope.user.status0 = true;
                        break;
                    case 1:
                        $scope.user.status = "未审核";
                        $scope.user.status1 = true;
                        break;
                    case 2:
                        $scope.user.status0 = false;
                        $scope.user.status1 = false;
                        break;
                }
            },_error);
        }
        /*选择门店刷新列表*/
        function _myTreeHandler (branch) {
            /*清理选中员工items*/
            items = {};
            if(branch.depId==0){
                //离职
                globalConfig.rootScope.paramMemberCondition.Status = "2";
                globalConfig.rootScope.paramMemberCondition.DepartmentId = " ";
                globalConfig.rootScope.paramMemberCondition.IsDimission = 'true';
                $scope.handle = [];
            }else if(branch.depId==-1){
                //未审核
                globalConfig.rootScope.paramMemberCondition.Status = "1";
                globalConfig.rootScope.paramMemberCondition.DepartmentId = " ";
                globalConfig.rootScope.paramMemberCondition.IsDimission = 'false';
                $scope.handle = [];
                var handle1_1 = handle1;
                handle1_1.name = "删除选中成员";
                $scope.handle.push(handle1_1);
                $scope.handle.push(handle2);
            }else if(branch.depId==-2){
                //未激活
                globalConfig.rootScope.paramMemberCondition.DepartmentId = " ";
                globalConfig.rootScope.paramMemberCondition.Status = "0";
                globalConfig.rootScope.paramMemberCondition.IsDimission = 'false';
                $scope.handle = [];
                var handle1_1 = handle1;
                handle1_1.name = "删除选中成员";
                $scope.handle.push(handle1_1);
            }else{
                globalConfig.rootScope.paramMemberCondition.Status = " ";
                globalConfig.rootScope.paramMemberCondition.DepartmentId = branch.depId;
                globalConfig.rootScope.paramMemberCondition.IsDimission = 'false';
                Department = branch;
                $scope.handle = [];

                $scope.handle.push(handle1);
                $scope.handle.push(handle3);
            }
            $scope.depName = branch.label;
            globalConfig.rootScope.paramMemberCondition.CurrentPage = 1;
            var member = $('#member');
            member.removeClass('show');
            _getMemberList();
        }
        /*角色搜索*/
        function _byRoleHandler(){
            if($scope.selected!=null) {
                globalConfig.rootScope.paramMemberCondition.RoleId = $scope.selected.id;
            }else{
                globalConfig.rootScope.paramMemberCondition.RoleId = " ";
            }
            globalConfig.rootScope.paramMemberCondition.CurrentPage = 1;
            _getMemberList();
        }
        /*姓名\电话\职位搜索*/
        function _byNameHandler() {
            if($scope.people_name === undefined||$scope.people_name==''){
                globalConfig.rootScope.paramMemberCondition.Name=" ";
            }else {
                globalConfig.rootScope.paramMemberCondition.Name = $scope.people_name.replace(/[ ]/g,"");
            }
            if($scope.people_phone === undefined||$scope.people_phone==''){
                globalConfig.rootScope.paramMemberCondition.Phone=" ";
            }else {
                globalConfig.rootScope.paramMemberCondition.Phone = $scope.people_phone.replace(/[ ]/g,"");
            }
            if($scope.people_position === undefined||$scope.people_position==''){
                globalConfig.rootScope.paramMemberCondition.Position=" ";
            }else {
                globalConfig.rootScope.paramMemberCondition.Position = $scope.people_position.replace(/[ ]/g,"");
            }
            globalConfig.rootScope.paramMemberCondition.CurrentPage = 1;
            _getMemberList();
        };
        /*翻页*/
        function _changePageHandler() {
            globalConfig.rootScope.paramMemberCondition.CurrentPage = $scope.Page;
            _getMemberList();
        };
        /*创建日期排序*/
        function _sortOrderHandler() {
            var sortOrder = globalConfig.rootScope.paramMemberCondition.SortOrder;
            globalConfig.rootScope.paramMemberCondition.SortOrder = !sortOrder?1:0;
            _getMemberList();
        };
        /*获取部门*/
        function _getDepartmentList(){

            var list = companyService.getDepartment(globalConfig.rootScope.paramMemberCondition.CompanyId);
            list.then(function(data){
                    $scope.doing_async = false;
                    $scope.groups = data.tree;
                    if(globalConfig.rootScope.paramMemberCondition.DepartmentId==" "){
                        globalConfig.rootScope.paramMemberCondition.DepartmentId = data.tree[0].depId;
                        $scope.depName = data.tree[0].label;
                    }
                    return $timeout(
                        function() {
                            /*默认选择tree的第一个*/
                            tree.select_first_branch();
                            /*展开全部*/
                            tree.expand_all();
                            /*新增离职、未审核、未激活*/
                            var b = tree.get_selected_branch();
                            tree.add_branch(b, {
                                label: '离职人员',
                                depId: 0
                            });
                            tree.add_branch(b, {
                                label: '未审核',
                                depId: -1
                            });
                            tree.add_branch(b, {
                                label: '未激活',
                                depId: -2
                            });
                        }, 300);
                },_error
            );
        }
        /*打开成员详情页*/
        function _openMemberInfo(id){
            $state.go('admin.member.info', {data: id});
        }

        /*判断是否已选择*/
        function _judgeChecked(list){
            angular.forEach(list,function(val1,key1){
                val1.checked = false;
                angular.forEach(items,function(val2,key2){
                    if(val1.id==val2){
                        val1.checked = true;
                    }
                });
            });
            return list;
        }
        /*选择成员*/
        function _isCheck(){
            var key = this.i.id;
            if(this.i.checked){
                items[key] = this.i.id;
            }else{
                $scope.checkAll = false;
                delete items[key];
            }
        }
        function _checkedAll(){
            var checkAllDepartment = $scope.checkAll;
            if(checkAllDepartment) {
                angular.forEach($scope.MemberList, function (val, key) {
                    if(!val.checked){
                        val.checked = !val.checked;
                        items[val.id] = val.id;
                    }
                });
            }else{
                angular.forEach($scope.MemberList, function (val, key) {
                    if(val.checked){
                        val.checked = !val.checked;
                        delete items[val.id];
                    }
                });
            }
        }


        /*短信提醒激活*/
        function _sendSMS(MemberId) {
            var data ={};
            data.CompanyId = currentUser.company.id;
            data.MemberId = MemberId;
            var sendSMS = companyService.sendSMS(data);
            sendSMS.then(function(){
                _success('已发送短信提醒激活');
            },_error);
        }

        /*设置电子档案*/
        function _setArchives(MemberId) {
            var data ={};
            data.CompanyId = currentUser.company.id;
            data.MemberId = MemberId;
            var sendSMS = companyService.setMemberArchives(data);
            sendSMS.then(function(){
                _success('开始导出');
            },_error);
        }

        /*打开弹出框*/
        function _openControl (size) {
            if($scope.controller!=null) {
                var modal_url,modal_ctl;
                var modal_data = {};
                if($scope.controller.id==1) {
                    modal_url = "modalSetDimission.html";
                    modal_ctl = "ModalSetDimissionCtrl";
                    modal_data.controller = $scope.controller;
                }else if($scope.controller.id==2){
                    modal_url = "modalComfirmMember.html";
                    modal_ctl = "ModalComfirmMemberCtrl";
                    modal_data.controller = $scope.controller;
                    modal_data.roles = $scope.role;
                    modal_data.menberId = items;
                }else if($scope.controller.id==3){
                    modal_url = "modalTransferDepartment.html";
                    modal_ctl = "ModalTransferDepartmentCtrl";
                    modal_data.controller = $scope.controller;
                    size = 'sm'
                }
                var modalInstance = $modal.open({
                    animation: true,
                    templateUrl: modal_url,
                    controller: modal_ctl,
                    size: size,
                    backdrop:'static',
                    resolve: {
                        modalData: function () {
                            return modal_data;
                        }
                    }
                });

                modalInstance.result.then(function (data) {
                    if($scope.controller.id==1) {
                        var arry = [];
                        angular.forEach(items, function (value, key) {
                            arry.push(value);
                        },true);
                        /*此处暂时设置转授人id为5*/
                        _setDimission(5,arry,data.code);
                    }else if($scope.controller.id==2) {
                        /*angular.forEach(items, function (value, key) {
                            _comfirmMember(parseInt(value), true,data.roles);
                        })*/
                        _getMemberList();
                    }else if($scope.controller.id==3){
                        var arry = [];
                        angular.forEach(items, function (value, key) {
                            arry.push(value);
                        },true);
                        _transferDepartment(arry,data.depId);
                    }
                }, _error);
            }
        }

        /*部门管理弹框*/
        function _openForExitDepartment(size){
            var modal_data = {};
            modal_data.department = Department;
            modal_data.departmentList = $scope.groups;
            var modalInstance = $modal.open({
                animation: true,
                templateUrl: 'modalExitDepartment.html',
                controller: 'ModalExitDepartmentCtrl',
                size: size,
                backdrop:'static',
                resolve: {
                    modalData: function () {
                        return modal_data;
                    }
                }
            });
            modalInstance.result.then(init);
        }

        /*批量导入人员弹框*/
        function _openForCvs(size){
            var modal_data = {};
            var modalInstance = $modal.open({
                animation: true,
                templateUrl: 'modalCvs.html',
                controller: 'ModalCvsCtrl',
                size: size,
                backdrop:'static',
                resolve: {
                    modalData: function () {
                        return modal_data;
                    }
                }
            });
            modalInstance.result.then(_getMemberList);
        }
        /*导出人员列表弹框*/
        function _openForExportCvs(size){
            var modal_data = {};
            var modalInstance = $modal.open({
                animation: true,
                templateUrl: 'modalExportCvs.html',
                controller: 'ModalExportCvsCtrl',
                size: size,
                backdrop:'static',
                resolve: {
                    modalData: function () {
                        return modal_data;
                    }
                }
            });
            modalInstance.result.then(
                function(){
                    _success('开始导出');
                    _getMemberList();
                },_error);
        }

        /*成员编辑弹框*/
        function _openForexitMember(MemberId){
            var modal_data = {};
            modal_data.department = Department;
            modal_data.MemberId = MemberId;
            var modalInstance = $modal.open({
                animation: true,
                templateUrl: 'modalExitMember.html',
                controller: 'ModalExitMemberCtrl',
                size: 'lg',
                backdrop:'static',
                resolve: {
                    modalData: function () {
                        return modal_data;
                    }
                }
            });

            modalInstance.result.then(function () {
                var member = $('#member');
                member.removeClass('show');
                _getMemberList();
            });
        }

        /*成员删除弹框*/
        function _openForDeleteMember(MemberId){
            var modal_data = {};
            modal_data.department = Department;
            modal_data.MemberId = MemberId;
            var modalInstance = $modal.open({
                animation: true,
                templateUrl: 'modalDeleteMember.html',
                controller: 'ModalDeleteMemberCtrl',
                size: '',
                backdrop:'static',
                resolve: {
                    modalData: function () {
                        return modal_data;
                    }
                }
            });

            modalInstance.result.then(function (form) {
                    var member = $('#member');
                    member.removeClass('show');
                    _getMemberList();
            }, _error);
        }
        /*邀请成员弹框*/
        function _openForaddMember(){
            var modal_data = {};
            var modalInstance = $modal.open({
                animation: true,
                templateUrl: 'modalAddMember.html',
                controller: 'ModalAddMemberCtrl',
                size: '',
                backdrop:'static',
                resolve: {
                    modalData: function () {
                        return modal_data;
                    }
                }
            });
        }
        /*获取成员列表*/
        function _getMemberList(){
            var memberList = companyService.getCompanyMemberList(globalConfig.rootScope.paramMemberCondition);
            memberList.then(function(data){
                $scope.MemberList = _judgeChecked(data.list);
                $scope.bigTotalItems = data.listcount;
                angular.forEach($scope.MemberList,function(val,key){
                    val.genderVal = dictionaryService.Gender()[val.gender].Description;
                });
            },_error);
        }
        /*审核成员
         * @param mid 员工id
         * @param isComfirm 是否通过审核
         * */
        function _comfirmMember(mid,isComfirm,roles){
            var data = {};
            data.CompanyId = currentUser.company.id;
            data.MemberId = mid;
            data.Confirmed = isComfirm;
            var comfirmMember = companyService.comfirmMember(data);
            comfirmMember.then(function(){
                data.RoleId = roles;
                _setRole(data);
            },_error);
        }
        function _openForcomfirmMember (size) {
            var modal_url,modal_ctl;
            var modal_data = {};
            var memberInfo = this.user;
            modal_data.controller = {};
            modal_url = "modalComfirmMember.html";
            modal_ctl = "ModalComfirmMemberCtrl";
            modal_data.controller.name = '成员审核';
            modal_data.roles = $scope.role;
            modal_data.menberId = [];
            modal_data.menberId.push(this.user.id);
            var modalInstance = $modal.open({
                animation: true,
                templateUrl: modal_url,
                controller: modal_ctl,
                size: size,
                backdrop:'static',
                resolve: {
                    modalData: function () {
                        return modal_data;
                    }
                }
            });

            modalInstance.result.then(function (data) {
                //_comfirmMember(memberInfo.id, true,data.roles);
                _getMemberList();
            }, _error);
        }
        /*分配角色*/
        function _setRole(data){
            var exitRole = companyService.exitMemberRole(data);
            exitRole.then(function(){
                _success('操作成功');
                _getMemberList();
            },_error);
        }
        /*批量離職
         * @param tId 转授人id
         * @param mid list员工id
         * @param code 验证码
        * */
        function _setDimission(tId,mId,code){
            var data = {};
            data.CompanyId = currentUser.company.id;
            data.TransferId = tId;
            data.MemberId = mId;
            data.Code = code;
            var setDimission = companyService.setDimission(data);
            setDimission.then(function(data){
                _success('操作成功');
                _getMemberList();
            },_error);
        }
        /*批量转移部门
         * @param mId list员工id
         * @param dId 新部门id
         * */
        function _transferDepartment(mId,dId){
            var data = {};
            data.CompanyId = currentUser.company.id;
            data.MemberId = mId;
            data.DepartmentId = dId;
            data.OldDepartmentId = globalConfig.rootScope.paramMemberCondition.DepartmentId;
            var transferDepartment = companyService.transferDepartment(data);
            transferDepartment.then(function(data){
                _success('操作成功');
                _getMemberList();
            },_error);
        }
        /*获取角色
        * @param cid 公司id
        * */
        function _getRoleList(cid){
            var getRoleList = companyService.getRoleList(cid);
            getRoleList.then(function(data){
                $scope.role = data;
            },_error);
        }

        function _success(msg){
            globalConfig.rootScope.successMessage = msg;
            globalConfig.rootScope.errorMessage = '';
        }
        function _error(err){
            if(err) {
                globalConfig.rootScope.successMessage = '';
                globalConfig.rootScope.errorMessage = err.message.join('&lt;br&frasl;&gt;');
            }
        }
    }
});