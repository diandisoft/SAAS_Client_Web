/**
 * Created by jianbo on 2016/1/19.
 * 公司管理
 */

'use strict';

define(['app',"../companyService"],function (app) {

    /*成员退出角色操作弹出款Ctl*/
    app.controller('ModalBatchOutRoleController',controlBatchOutRole);
    controlBatchOutRole.$inject = ['globalConfig','$scope','$uibModalInstance','modalData','$validation','currentUserService','companyService','$timeout','$stateParams'];
    /*添加其他成员*/
    app.controller('ModalSelectOtherMembersCtrl',controlSelectOtherMembers);
    controlSelectOtherMembers.$inject = ['globalConfig','$scope','$uibModalInstance','modalData','currentUserService','companyService','$timeout'];

    app.controller('RoleMemberListController',control);
    control.$inject = ['globalConfig','$scope','$state','companyService','$stateParams','currentUserService','$modal','dictionaryService'];


    function controlBatchOutRole(globalConfig,$scope, $uibModalInstance,modalData,$validationProvider,currentUserService,companyService,$timeout,$stateParams) {
        $scope.title = modalData.controller.name;
        $scope.message = modalData.message;
        $scope.checkValid = $validationProvider.checkValid;
            $scope.ok = function(form){
                currentUserService.currentUser().then(function(currentUser) {
                    form.CompanyId = currentUser.company.id;
                    form.RoleId = $stateParams.RoleId;
                    form.MemberId = modalData.memberId;
                    var batchOutRole = companyService.batchOutRole(form);
                    batchOutRole.then(
                        function() {
                            _success("成功移出");
                            $timeout(function () {
                                $uibModalInstance.close(form);
                            }, 2000);
                        }, _error);
                },_error);
            }
            $scope.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            }
        function _success(msg){
            globalConfig.rootScope.successMessage = msg;
            globalConfig.rootScope.errorMessage = '';
        }
        function _error(err){
            if(err) {
                globalConfig.rootScope.successMessage = '';
                globalConfig.rootScope.errorMessage = err.message.join('&lt;br&frasl;&gt;');
            }
        }
    }

    function controlSelectOtherMembers(globalConfig,$scope, $uibModalInstance,modalData,currentUserService,companyService,$timeout) {
        var tree,Department,currentUser,member;
        var items = {};
        /*成员列表初始条件*/
        var paramMemberCondition = {
            CompanyId:" ",
            Name:" ",
            Phone:" ",
            Position:" ",
            RoleId:" ",
            DepartmentId:" ",
            Status:"2",
            IsDimission:"false",
            SortOrder:"0",
            CurrentPage:1,
            PageSize:10
        };
        $scope.numPages = 5;//分页显示数量
        $scope.pageSize = paramMemberCondition.PageSize;
        $scope.doing_async1 = true;
        $scope.depList=[];
        $scope.my_tree1 = tree = {};
        $scope.title = modalData.controller.name;
        $scope.myTreeHandler1 = _myTreeHandler1;
        $scope.changePageHandler = _changePageHandler;
        $scope.byNameHandler1 = _byNameHandler1;
        $scope.byRoleHandler1 = _byRoleHandler1;
        $scope.isCheck = _isCheck;
        $scope.checkedAll = _checkedAll;
        init();

        function init() {
            currentUserService.currentUser().then(function(data){
                currentUser = data;
                paramMemberCondition.CompanyId = currentUser.company.id;
                paramMemberCondition.Position = " ";
                _getRolt(currentUser.company.id);
                _getDepartmentList();
            },_error);
        }

        function _getDepartmentList(){
            var list = companyService.getDepartment(currentUser.company.id);
            list.then(
                function (data) {
                    $scope.depList = data.tree;
                    $scope.doing_async1 = false;
                    return $timeout(
                        function() {
                            tree.select_first_branch();
                            tree.expand_all();
                        },300);
                }
            );
        }

        function _myTreeHandler1 (branch) {
            paramMemberCondition.DepartmentId = branch.depId;
            paramMemberCondition.IsDimission = 'false';
            Department = branch;
            $scope.depName = branch.label;
            paramMemberCondition.CurrentPage = 1;
            _getMemberList();
        }

        /*姓名\电话\职位搜索*/
        function _byNameHandler1(vm) {
            if(vm.people_name === undefined||vm.people_name==''){
                paramMemberCondition.Name=" ";
            }else {
                paramMemberCondition.Name = vm.people_name.replace(/[ ]/g,"");
            }
            if(vm.people_phone === undefined||vm.people_phone==''){
                paramMemberCondition.Phone=" ";
            }else {
                paramMemberCondition.Phone = vm.people_phone.replace(/[ ]/g,"");
            }
            if(vm.people_position === undefined||vm.people_position==''){
                paramMemberCondition.Position=" ";
            }else {
                paramMemberCondition.Position = vm.people_position.replace(/[ ]/g,"");
            }
            paramMemberCondition.CurrentPage = 1;
            _getMemberList();
        }
        /*角色搜索*/
        function _byRoleHandler1(selected){
            if(selected!=null) {
                paramMemberCondition.RoleId = selected.id;
            }else{
                paramMemberCondition.RoleId = " ";
            }
            paramMemberCondition.CurrentPage = 1;
            _getMemberList();
        }
        function _getRolt(cid) {
            var getRoltList = companyService.getRoleList(cid);
            getRoltList.then(function (data) {
                $scope.roles = data.list;
            }, _error);
        }
        /*获取成员列表*/
        function _getMemberList(){
            var memberList = companyService.getCompanyMemberList(paramMemberCondition);
            memberList.then(function(data){
                $scope.MemberList = _judgeChecked(data.list);
                $scope.bigTotalItems = data.listcount;
            },_error);
        }
        /*翻页*/
        function _changePageHandler() {
            paramMemberCondition.CurrentPage = this.P;
            _getMemberList();
        }

        /*判断是否已选择*/
        function _judgeChecked(list){
            angular.forEach(list,function(val1,key1){
                val1.checked = false;
                angular.forEach(items,function(val2,key2){
                    if(val1.id==val2){
                        val1.checked = true;
                    }
                });
            });
            return list;
        }
        /*选择成员*/
        function _isCheck(){
            var key = this.i.id;
            if(this.i.checked){
                items[key] = this.i.id;
            }else{
                delete items[key];
                $scope.checkAllMember = false;
            }
        }
        function _checkedAll(){
            var checkAllMember = this.checkAllMember;
            if(checkAllMember) {
                angular.forEach($scope.MemberList, function (val, key) {
                    if(!val.checked){
                        val.checked = !val.checked;
                        items[val.id] = val.id;
                    }
                });
            }else{
                angular.forEach($scope.MemberList, function (val, key) {
                    if(val.checked){
                        val.checked = !val.checked;
                        delete items[val.id];
                    }
                });
            }
        }

        $scope.ok = function () {
            $uibModalInstance.close(items);
        };
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
        function _error(err){
            if(err) {
                globalConfig.rootScope.successMessage = '';
                globalConfig.rootScope.errorMessage = err.message.join('&lt;br&frasl;&gt;');
            }
        }
    }

    function control(globalConfig,$scope,$state,companyService,$stateParams,currentUserService,$modal,dictionaryService){
        var items = {};
        var currentUser;
        var paramMemberCondition={
            CompanyId:"",
            RoleId:"",
            IsDimission:"false",
            CurrentPage:1,
            PageSize:10,
            Name:"",
            Phone:"",
            Position:""
        };
        var vm = {
            CompanyId:"",
            RoleId:""
        };
        var handle1 = {"name":"选中成员从角色移出","id":1};
        globalConfig.rootScope.app.title = '角色人员管理 - 点滴科技经纪云办公平台';

        $scope.numPages = 5;//分页显示数量
        $scope.changePageHandler = _changePageHandler;
        $scope.openMember = _openMember;
        $scope.handle = [];
        $scope.byNameHandler = _byNameHandler;
        $scope.openControl = _openControl;
        $scope.isCheck = _isCheck;
        $scope.checkedAll = _checkedAll;
        $scope.batchOutRoleForOne = _batchOutRoleForOne;
        $scope.selectMembers = _selectMembers;

        /*初始化*/
        init();
        function init() {
            currentUserService.currentUser().then(function(data){
                currentUser = data;
                vm.CompanyId = currentUser.company.id;
                vm.RoleId = $stateParams.RoleId;
                $scope.handle.push(handle1);
                _getRoleMemberList(vm);
                _getRoleInfo(vm);
            },_error);

        }

        /*获取角色
         * @param vm.CompanyId 公司id
         * @param vm.RoleId 角色id
         * */
        function _getRoleInfo(vm){
            var getRole = companyService.getRole(vm);
            getRole.then(function(data){
                $scope.role = data.roleinfo;
            },_error);
        }
        function _getRoleMemberList(value){
            paramMemberCondition.CompanyId = value.CompanyId;
            paramMemberCondition.RoleId = value.RoleId;
            paramMemberCondition.IsDimission = false;
            var memberList = companyService.getCompanyMemberList(paramMemberCondition);
            memberList.then(function(data){
                $scope.member = data;
                $scope.member.list = _judgeChecked($scope.member.list);
                angular.forEach($scope.member.list,function(val,key){
                    val.genderVal = dictionaryService.Gender()[val.gender].Description;
                });
            },_error);
        }
        /*翻页*/
        function _changePageHandler() {
            paramMemberCondition.CurrentPage = $scope.Page;
            _getRoleMemberList(vm);
        };
        /*跳转至员工详情*/
        function _openMember(){
            $state.go('admin.member.info', {data: this.i.id});
        }
        /*批量成员退出角色*/
        function _batchOutRole(data){
            var batchOutRole = companyService.batchOutRole(data);
            batchOutRole.then(
                function () {
                    _success("成功从["+$scope.role.name+"]移出");
                    _getRoleMemberList(vm);
                },_error);
        }
        /*单个成员退出角色*/
        function _batchOutRoleForOne(size){
            var modal_url,modal_ctl;
            var modal_data = {};
            modal_data.controller = {};
            modal_data.memberId = [];
            modal_url = "modalBatchOutRole.html";
            modal_ctl = "ModalBatchOutRoleController";
            modal_data.controller.name = '移出角色';
            modal_data.message = "请输入验证码，确定该成员从["+$scope.role.name+"]移出";
            modal_data.memberId.push(this.i.id);
            var modalInstance = $modal.open({
                animation: false,
                templateUrl: modal_url,
                controller: modal_ctl,
                size: size,
                backdrop:'static',
                resolve: {
                    modalData: function () {
                        return modal_data;
                    }
                }
            });

            modalInstance.result.then(function (form) {
                _getRoleMemberList(vm);
            }, _error);
        }
        /*姓名\电话\职位搜索*/
        function _byNameHandler() {
            if($scope.people_name === undefined||$scope.people_name==''){
                paramMemberCondition.Name=" ";
            }else {
                paramMemberCondition.Name = $scope.people_name.replace(/[ ]/g,"");
            }
            if($scope.people_phone === undefined||$scope.people_phone==''){
                paramMemberCondition.Phone=" ";
            }else {
                paramMemberCondition.Phone = $scope.people_phone.replace(/[ ]/g,"");
            }
            if($scope.people_position === undefined||$scope.people_position==''){
                paramMemberCondition.Position=" ";
            }else {
                paramMemberCondition.Position = $scope.people_position.replace(/[ ]/g,"");
            }
            paramMemberCondition.CurrentPage = 1;
            _getRoleMemberList(vm);
        };
        /*判断是否已选择*/
        function _judgeChecked(list){
            angular.forEach(list,function(val1,key1){
                val1.checked = false;
                angular.forEach(items,function(val2,key2){
                    if(val1.id==val2){
                        val1.checked = true;
                    }
                });
            });
            return list;
        }
        /*选择成员*/
        function _isCheck(){
            var key = this.i.id;
            if(this.i.checked){
                items[key] = this.i.id;
            }else{
                delete items[key];
                $scope.checkAll = false;
            }
        }
        function _checkedAll(){
            var checkAllMember = $scope.checkAll;
            if(checkAllMember) {
                angular.forEach($scope.member.list, function (val, key) {
                    if(!val.checked){
                        val.checked = !val.checked;
                        items[val.id] = val.id;
                    }
                });
            }else{
                angular.forEach($scope.member.list, function (val, key) {
                    if(val.checked){
                        val.checked = !val.checked;
                        delete items[val.id];
                    }
                });
            }
        }
        function _openControl (size) {
            if($scope.controller!=null) {
                var modal_url,modal_ctl;
                var modal_data = {};
                if($scope.controller.id==1) {
                    modal_url = "modalBatchOutRole.html";
                    modal_ctl = "ModalBatchOutRoleController";
                    modal_data.controller = $scope.controller;
                    modal_data.message = "请输入验证码，确定选择成员从["+$scope.role.name+"]移出";
                    modal_data.memberId = [];
                    angular.forEach(items,function(val,key){
                        modal_data.memberId.push(val)
                    });
                }
                var modalInstance = $modal.open({
                    animation: false,
                    templateUrl: modal_url,
                    controller: modal_ctl,
                    size: size,
                    backdrop:'static',
                    resolve: {
                        modalData: function () {
                            return modal_data;
                        }
                    }
                });

                modalInstance.result.then(function (form) {
                    _getRoleMemberList(vm);
                }, _error);
            }
        }

        function _selectMembers(){
            var modal_data = {
                controller:{
                    name:''
                }
            };
            var modal_url = "modalSelectOtherMembers.html";
            var modal_ctl = "ModalSelectOtherMembersCtrl";
            modal_data.controller.name = '选择成员';
            var size = 'lg';
            var modalInstance = $modal.open({
                animation: true,
                templateUrl: modal_url,
                controller: modal_ctl,
                size: size,
                backdrop:'static',
                resolve: {
                    modalData: function () {
                        return modal_data;
                    }
                }
            });
            modalInstance.result.then(function (data) {
                vm.memberId = [];
                angular.forEach(data,function(val,key){
                    vm.memberId.push(val);
                });
                var batchJoinRole = companyService.batchJoinRole(vm);
                batchJoinRole.then(function(){
                    _success('操作成功');
                    _getRoleMemberList(vm);
                },_error);
            }, _error);
        }

        function _success(msg){
            globalConfig.rootScope.successMessage = msg;
            globalConfig.rootScope.errorMessage = '';
        }
        function _error(err){
            if(err) {
                globalConfig.rootScope.successMessage = '';
                globalConfig.rootScope.errorMessage = err.message.join('&lt;br&frasl;&gt;');
            }
        }
    }
});