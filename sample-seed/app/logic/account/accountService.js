/**
 * Created by mybbcat on 2015/12/10.
 * 账号相关的逻辑服务
 */

define(['app'], function (app) {

    app.factory('accountService', ['globalConfig','$http','$q', function (globalConfig,$http,$q) {
        var serviceFactory = {};

        serviceFactory.smsCode = _getSmsCode;
        serviceFactory.signinCompany = _postSignInCompany;
        serviceFactory.signUp =_signUp;
        serviceFactory.getCompanySimpleInfor = _getCompanySimpleInfor;
        serviceFactory.exitMember = _exitMember;

        /**
         * 请求向手机发送一个短信验证码
         * @param phoneNo 手机号码
         * @param securityCode 安全验证码
         * @private
         */
        function _getSmsCode(phoneNo, securityCode){
            var deferred = $q.defer();

            var url = globalConfig.apiUrl.account.getSmsSecurityCode.format(phoneNo,securityCode);

            $http.get(url).success(function(response){
                    deferred.resolve(response);
                }).error(function(err){
                    deferred.reject(err)
            });
            return deferred.promise;
        }

        /**
         * 注册企业账号
         * @param data addCompanyVm
         * @returns {*}
         * @private
         */
        function _postSignInCompany(data){
            var deferred = $q.defer();

            var url = globalConfig.apiUrl.account.registerCompany;

            $http.post(url, data).success(function(response){
                deferred.resolve(response);
            }).error(function(err){
                deferred.reject(err)
            });
            return deferred.promise;
        }


        /*注册成员*/
        function _signUp(data){
            //todo 注册
            var deferred = $q.defer();
            var url = globalConfig.apiUrl.account.registerMember.format(data.companyId);
            $http.post(url,data).success(function(response){
                deferred.resolve(response);
            }).error(function(err){
                deferred.reject(err)
            });
            return deferred.promise;
        }

        /**
         *获取公司信息
         * @param companyId 公司id
         **/
        function _getCompanySimpleInfor(companyId){
            var deferred = $q.defer();
            var url = globalConfig.apiUrl.company.getCompanySimpleInfor.format(companyId);
            $http.get(url).success(function(response){
                deferred.resolve(response);
            }).error(function(err){
                deferred.reject(err)
            });
            return deferred.promise;
        }

        /**
         *修改成员信息
         * @param companyId 公司id
         **/
        function _exitMember(data){
            var deferred = $q.defer();
            var url = globalConfig.apiUrl.account.exitMember.format(data.CompanyId,data.MemberId);
            $http.put(url,data).success(function(response){
                deferred.resolve(response);
            }).error(function(err){
                deferred.reject(err)
            });
            return deferred.promise;
        }


        return serviceFactory;
    }]);
});