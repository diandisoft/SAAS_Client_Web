/**
 * Created by mybbcat on 2015/12/8.
 * 注册新成员账号，加入已存在的公司
 */
'use strict';

define(["app","../accountService"], function (app) {

    app.controller('companySimpleInforController',control);
    control.$inject = ['globalConfig','$scope','underscoreJS','accountService','$validation','$state'];

    function control(globalConfig,$scope,underscoreJS,accountService,$validationProvider,$state){

        $scope.vm = {
            companyId :''
        };

        $scope.viewBag = {
            securityCodeError: false
        };

        $scope.getCompanySimpleInfor = _getCompanySimpleInfor;
        $scope.checkValid = $validationProvider.checkValid;


        function _getCompanySimpleInfor(){
            var companyId = this.vm.companyId;
            var Company = accountService.getCompanySimpleInfor(companyId);
            Company.then(_addCompanyAccountSuccess,_addCompanyAccountFail);
            return Company;
        }
        function _addCompanyAccountSuccess(data){
            globalConfig.rootScope.errorMessage = '';
            var company = data.company.id;
            underscoreJS.delay(function(){$state.go('access.signupUser', {data: company})},3000);
        }

        function _addCompanyAccountFail(err){
            if (err) {
                globalConfig.rootScope.successMessage = "";
                globalConfig.rootScope.errorMessage = err.message.join('&lt;br&frasl;&gt;');
            }
        }
    }
});