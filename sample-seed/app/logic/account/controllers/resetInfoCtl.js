/**
 * Created by mybbcat on 2015/12/8.
 * 被邀请用户第一次登陆后，用户自行重设个人信息
 */
'use strict';

define(["app","../accountService"], function (app) {

    app.controller('resetInfoController',control);
    control.$inject = ['globalConfig','$scope','underscoreJS','accountService','$validation','$state','currentUserService'];

    function control(globalConfig,$scope,underscoreJS,accountService,$validationProvider,$state,currentUserService){

        $scope.vm = {
            companyId :''
        };

        $scope.viewBag = {
            securityCodeError: false
        };

        $scope.resetInfo = _resetInfo;
        $scope.checkValid = $validationProvider.checkValid;
        var currentUser;
        var verificationMe = currentUserService.currentUser();
        verificationMe.then(function(response){
            currentUser = response;
        },_addCompanyAccountFail);
        function _resetInfo(vm){
            vm.MemberId = currentUser.id;
            vm.CompanyId = currentUser.company.id;
            var exitMember = accountService.exitMember(vm);
            exitMember.then(_addCompanyAccountSuccess,_addCompanyAccountFail);
        }
        function _addCompanyAccountSuccess(data){
            globalConfig.rootScope.errorMessage = '';
            globalConfig.rootScope.successMessage = "设置成功，进入首页中…";
            underscoreJS.delay(function(){$state.go('home.dashboard')},3000);
        }

        $scope.open = function($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.opened = true;
        };

        function _addCompanyAccountFail(err){
            if (err) {
                globalConfig.rootScope.successMessage = "";
                globalConfig.rootScope.errorMessage = err.message.join('&lt;br&frasl;&gt;');
            }
        }
    }
});