/**

 * Created by mybbcat on 2015/12/8.
 * 成员登陆

 */

'use strict';

define(['app'],function (app) {

    app.controller('signinController',control);
    control.$inject = ['globalConfig','$scope','$state','cryptoJS','authService','currentUserService'];

    function control(globalConfig,$scope,$state,cryptoJS,authService,currentUserService){

        globalConfig.rootScope.app.title = '登录 - 点滴科技经纪云办公平台';

        $scope.viewBag = {error: ''};

        $scope.signIn = function(username, password){
            var a =  authService.signIn(username, cryptoJS.MD5(password).toString());
            a.then(verification ,signIn_Fail);
            return a;
        }

        function verification(){
            var verificationMe = currentUserService.currentUser();
            verificationMe.then(
                function(data){
                    /*判断是否第一次登录并激活*/
                    if(data.firstlogin){
                        $state.go('access.resetInfo');
                    }else{
                        signIn_Success();
                    }
                },signIn_Fail
            )
        }

        var signIn_Success = function(){
            $state.go('home.dashboard');
        };

        var signIn_Fail = function(err){
            if (err) {
                $scope.viewBag.error = err.message.join('<br/>');
            }
        }

    };
});
