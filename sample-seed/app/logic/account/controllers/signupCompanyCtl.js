/**
 * Created by mybbcat on 2015/12/8.
 * 注册新公司
 */

"use strict";

define(["app","../accountService"], function (app) {

    app.controller('signupCompanyController',control);
    control.$inject = ['globalConfig','$scope','underscoreJS','accountService','$validation','$state','cryptoJS'];

    function control(globalConfig,$scope,underscoreJS,accountService,$validationProvider,$state,cryptoJS){

        $scope.vm = {
            name: '',
            Phone: '',
            password: '',
            smsCode: '',
            securityCode: '',
            agree: ""
        };

        $scope.viewBag = {
            securityCodeError: false
        };

        $scope.requestSmsCode = _requestSmsCode;
        $scope.requestAddCompany = _addCompanyAccount;
        $scope.checkValid = $validationProvider.checkValid;
        /**
         * 请求发送手机短信验证码
         * 必须校验图形验证码是正确的，才去请求短信
         */
        function _requestSmsCode() {
            if(underscoreJS.isEmpty(this.signupVm.securityCode)){
                this.viewBag.securityCodeError = true;
                globalConfig.rootScope.errorMessage = '请输入图形验证码！';
            }else{
                console.info(this.signupVm.userPhone);
                //todo 验证码这部分未实现
                accountService.smsCode(this.signupVm.userPhone, this.signupVm.securityCode);
            }
        }

        /**
         * 清求创建公司账号
         * @private
         */
        function _addCompanyAccount(){
            var data = {};
            data.name = this.vm.name;
            data.phone = this.vm.phone;
            data.password = cryptoJS.MD5(this.vm.password).toString();
            data.code = this.vm.smsCode;

            var Company = accountService.signinCompany(data).then(_addCompanyAccountSuccess ,_addCompanyAccountFail);
            return Company;
        }

        function _addCompanyAccountSuccess(response){
            globalConfig.rootScope.errorMessage = '';
            globalConfig.rootScope.successMessage = "成功注册企业账号！ 3秒后跳转至登录页...";
            underscoreJS.delay(function(){$state.go('access.signin');},3000);
        }

        function _addCompanyAccountFail(err){
            if (err) {
                globalConfig.rootScope.successMessage = '';
                globalConfig.rootScope.errorMessage = err.message.join('&lt;br&frasl;&gt;');
            }
        }
    }
});