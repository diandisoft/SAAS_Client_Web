/**
 * Created by mybbcat on 2015/12/8.
 * 注册新成员账号，加入已存在的公司
 */
'use strict';

define(["app","../accountService"], function (app) {

    app.controller('signupController',control);
    control.$inject = ['globalConfig','$scope','underscoreJS','accountService','$validation','$state','cryptoJS','$stateParams'];

    function control(globalConfig,$scope,underscoreJS,accountService,$validationProvider,$state,cryptoJS,$stateParams){

        $scope.vm = {
            departmentId :'',
            name: '',
            Phone: '',
            password: '',
            smsCode: '',
            securityCode: '',
            agree: ""
        };

        $scope.viewBag = {
            securityCodeError: false
        };

        /*初始化获取当前公司*/
        paramCompany();
        $scope.requestAddMember = _addCompanyMember;
        $scope.lastPase = _lastPase;
        $scope.checkValid = $validationProvider.checkValid;

        function _lastPase(){
            /*上一步*/
            $state.go('access.getCompanySimpleInfor');
        }

        /*获取公司*/
        function paramCompany(){
            var companyId = $stateParams.data;
            /*getCompanySimpleInfor的http请求已缓存*/
            var Company = accountService.getCompanySimpleInfor(companyId);
            Company.then(function(data){
                Company = data;
                $scope.company = data.company;
            },_addCompanyAccountFail);
        }

        /**
         * 清求注册成员
         * @private
         */
        function _addCompanyMember(vm){

            vm.companyId = $scope.company.id;
            vm.password = cryptoJS.MD5(this.vm.initialPassword).toString();

            var member = accountService.signUp(vm);
            member.then( _addCompanyAccountSuccess,_addCompanyAccountFail);

            return member;
        }

        function _addCompanyAccountSuccess(data){
            globalConfig.rootScope.errorMessage = '';
            globalConfig.rootScope.successMessage = "成功发送申请，请等待管理员["+$scope.company.adminName+"]审批";
            underscoreJS.delay(function(){$state.go('access.signin');},3000);
        }

        function _addCompanyAccountFail(err){
            if (err) {
                globalConfig.rootScope.successMessage = '';
                globalConfig.rootScope.errorMessage = err.message.join('&lt;br&frasl;&gt;');
            }
        }
    }
});