/**
 * Created by mybbcat on 2015/12/8.
 * 客户管理
 */
'use strict';

define(["app","../customerService"], function (app) {

    /*添加与修改客户操作弹出款Ctl*/
    app.controller('ModalAddAndExitCustomerCtrl',controlAddAndExitCustomer);
    controlAddAndExitCustomer.$inject = ['globalConfig','$scope','$uibModalInstance','$validation','currentUserService','modalData','customerService','$timeout'];
    app.controller('MyCustomerListController',control);
    control.$inject = ['globalConfig','$scope','customerService','$state','currentUserService','dictionaryService','$modal'];

    function controlAddAndExitCustomer(globalConfig,$scope, $uibModalInstance,$validationProvider,currentUserService,modalData,customerService,$timeout) {
        $scope.checkValid = $validationProvider.checkValid;
        $scope.title = modalData.title;
        $scope.contacts = [];
        $scope.addContacts = _addContacts;
        $scope.removeContacts = _removeContacts;
        if(modalData.customer === undefined){
            $scope.customer = {
                gender:0,
                status:0,
                level:0
            };
        }else{
            $scope.customer = modalData.customer;
            _setContacts(modalData.customer);
        }
        function _addContacts(){
            if($scope.contacts.length < 5) {
                var contacts = {
                    name: '',
                    tel: ''
                };
                $scope.contacts.push(contacts);
            }
        }
        function _removeContacts(){
            if($scope.contacts.length > 0) {
                $scope.contacts.pop();
            }
        }
        function _setContacts(form){
            $scope.contacts = [];
            if(form.contacts1!==undefined&form.contacts1!=''){
                var data = {};
                data.name = form.contacts1;
                data.tel = form.tel1;
                $scope.contacts.push(data);
            }if(form.contacts2!==undefined&form.contacts2!=''){
                var data = {};
                data.name = form.contacts2;
                data.tel = form.tel2;
                $scope.contacts.push(data);
            }if(form.contacts3!==undefined&form.contacts3!=''){
                var data = {};
                data.name = form.contacts3;
                data.tel = form.tel3;
                $scope.contacts.push(data);
            }if(form.contacts4!==undefined&form.contacts4!=''){
                var data = {};
                data.name = form.contacts4;
                data.tel = form.tel4;
                $scope.contacts.push(data);
            }if(form.contacts5!==undefined&form.contacts5!=''){
                var data = {};
                data.name = form.contacts5;
                data.tel = form.tel5;
                $scope.contacts.push(data);
            }
        }

        currentUserService.currentUser().then(function(currentUser) {
            $scope.ok = function (form) {
                form.contacts1 = $scope.contacts[0] ? $scope.contacts[0].name : '';
                form.contacts2 = $scope.contacts[1] ? $scope.contacts[1].name : '';
                form.contacts3 = $scope.contacts[2] ? $scope.contacts[2].name : '';
                form.contacts4 = $scope.contacts[3] ? $scope.contacts[3].name : '';
                form.contacts5 = $scope.contacts[4] ? $scope.contacts[4].name : '';
                form.tel1 = $scope.contacts[0] ? $scope.contacts[0].tel : '';
                form.tel2 = $scope.contacts[1] ? $scope.contacts[1].tel : '';
                form.tel3 = $scope.contacts[2] ? $scope.contacts[2].tel : '';
                form.tel4 = $scope.contacts[3] ? $scope.contacts[3].tel : '';
                form.tel5 = $scope.contacts[4] ? $scope.contacts[4].tel : '';
                form.CompanyId = currentUser.company.id;
                form.MemberId = currentUser.id;
                if(modalData.customer === undefined) {
                    var addCustomer = customerService.addCustomer(form);
                    addCustomer.then(
                        function () {
                            _success("新增成功");
                            $timeout(function (){
                                $uibModalInstance.close(form);
                            }, 2000);
                        }, _err);
                }else{
                    form.CustomerId = modalData.customer.id;
                    form.version = modalData.customer.version;
                    var addCustomer = customerService.exitCustomer(form);
                    addCustomer.then(
                        function(){
                            _success('编辑成功');
                            $timeout(function (){
                                $uibModalInstance.close(form);
                            }, 2000);
                        }, _err);
                }
            };
            $scope.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        });

        function _success(msg){
            globalConfig.rootScope.errorMessage = '';
            globalConfig.rootScope.successMessage = msg;
        }
        function _err(err){
            if(err) {
                globalConfig.rootScope.successMessage = '';
                globalConfig.rootScope.errorMessage = err.message.join('&lt;br&frasl;&gt;');
            }
        }
    }

    function control(globalConfig,$scope,customerService,$state,currentUserService,dictionaryService,$modal){
        var currentUser;
        /*条件*/
        var paramCustomerCondition = {
            CompanyId:'',
            MemberId:'',
            Name:'',
            Status:'',
            Level:'',
            Sort:0,
            SortOrder:0,
            CurrentPage:1,
            PageSize:20,
            Tel:''
        };
        $scope.changePageHandler = _changePageHandler;
        $scope.byNameHandler = _byNameHandler;
        $scope.openAddCustomerModal = _openAddCustomerModal;
        $scope.openExitCustomerModal = _openExitCustomerModal;
        $scope.openCustomer = _openCustomer;
        init();
        function init() {
            currentUserService.currentUser().then(function(respon){
                currentUser = respon;
                paramCustomerCondition.CompanyId = currentUser.company.id;
                paramCustomerCondition.MemberId = currentUser.id;
                $scope.numPages = 5;
                $scope.Page = 1;
                $scope.Pages = paramCustomerCondition.CurrentPage;
                $scope.pageSize = paramCustomerCondition.PageSize;
                _getMyCustomerList();
            },_err);
        }
        function _getMyCustomerList(){
            customerService.myCustomerList(paramCustomerCondition).then(function(respon){
                $scope.customercount = respon.customercount;
                $scope.pagecount = respon.pagecount;
                $scope.customerList = respon.list;
                $scope.bigTotalItems = respon.customercount;
                angular.forEach($scope.customerList,function(val,key){
                    val.levelVal = dictionaryService.Level()[val.level].Description;
                    val.genderVal = dictionaryService.Gender()[val.gender].Description;
                    val.statusVal = dictionaryService.CustomerStatus()[val.status].Description;
                })
            },_err);
        }
        /*姓名\电话搜索*/
        function _byNameHandler() {
            if($scope.people_name === undefined||$scope.people_name==''){
                paramCustomerCondition.Name=" ";
            }else {
                paramCustomerCondition.Name = $scope.people_name.replace(/[ ]/g,"");
            }
            if($scope.people_phone === undefined||$scope.people_phone==''){
                paramCustomerCondition.Tel=" ";
            }else {
                paramCustomerCondition.Tel = $scope.people_phone.replace(/[ ]/g,"");
            }
            paramCustomerCondition.CurrentPage = 1;
            _getMyCustomerList();
        };
        function _changePageHandler(){
            paramCustomerCondition.CurrentPage = $scope.Page;
            _getMyCustomerList();
        }
        /*打开新增客户弹出框*/
        function _openAddCustomerModal (size) {
            var modalInstance = $modal.open({
                animation: true,
                templateUrl: 'modalAddAndExitCustomer.html',
                controller: 'ModalAddAndExitCustomerCtrl',
                size: size,
                backdrop:'static',
                resolve: {
                    modalData:{
                        title:'新增客户'
                    }
                }
            });

            modalInstance.result.then(function (form) {
                _getMyCustomerList();
            }, _err);
        }
        /*打开编辑客户弹出框*/
        function _openExitCustomerModal (customer,size) {
            var exitData = {};
            exitData.CustomerId = customer.id;
            var modalInstance = $modal.open({
                animation: true,
                templateUrl: 'modalAddAndExitCustomer.html',
                controller: 'ModalAddAndExitCustomerCtrl',
                size: size,
                backdrop:'static',
                resolve: {
                    modalData:{
                        title:'编辑客户',
                        customer:customer
                    }
                }
            });

            modalInstance.result.then(function (form) {
                _getMyCustomerList();
                $('#customer').removeClass('show');
            }, _err);
        }
        /*打开客户详情*/
        function _openCustomer(){
            var member = $('#customer');
            member.addClass('show');
            var data = {};
            data.CompanyId = currentUser.company.id;
            data.MemberId = currentUser.id;
            data.CustomerId = this.i.id;
            var getCustomerInfo = customerService.getCustomerInfo(data);
            getCustomerInfo.then(function(data){
                $scope.customer = data.customerinfovm;
                $scope.customer.levelVal = dictionaryService.Level()[data.customerinfovm.level].Description;
                $scope.customer.genderVal = dictionaryService.Gender()[data.customerinfovm.gender].Description;
                $scope.customer.statusVal = dictionaryService.CustomerStatus()[data.customerinfovm.status].Description;
            },_err);
        }
        function _success(msg){
            globalConfig.rootScope.errorMessage = '';
            globalConfig.rootScope.successMessage = msg;
        }
        function _err(err){
            if(err) {
                globalConfig.rootScope.successMessage = '';
                globalConfig.rootScope.errorMessage = err.message.join('&lt;br&frasl;&gt;');
            }
        }
    }
});