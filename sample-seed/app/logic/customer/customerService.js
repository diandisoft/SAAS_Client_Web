/**
 * Created by jianbo on 2016/3/1.
 */
define(['app'], function (app) {

    app.factory('customerService', ['globalConfig','$http','$q', function (globalConfig,$http,$q) {
        var serviceFactory = {};
        serviceFactory.myCustomerList = _myCustomerList;
        serviceFactory.getCustomerInfo = _getCustomerInfo;
        serviceFactory.addCustomer = _addCustomer;
        serviceFactory.exitCustomer = _exitCustomer;
        /**
         * 获取我的客户列表
         * */
        function _myCustomerList(data){
            var deferred = $q.defer();
            var url = globalConfig.apiUrl.customer.myCustomerList.format(data.CompanyId,data.MemberId);
            $http({
                method:'GET',
                url:url,
                params:data
            }).success(function(response){
                deferred.resolve(response);
            }).error(function(err){
                deferred.reject(err)
            });
            return deferred.promise;
        }
        /**
         * 获取客户信息
         * */
        function _getCustomerInfo(data){
            var deferred = $q.defer();
            var url = globalConfig.apiUrl.customer.getCustomerInfo.format(data.CompanyId,data.MemberId,data.CustomerId);
            $http.get(url).success(function(response){
                deferred.resolve(response);
            }).error(function(err){
                deferred.reject(err)
            });
            return deferred.promise;
        }
        /**
         * 新增客户
         * */
        function _addCustomer(data){
            var deferred = $q.defer();
            var url = globalConfig.apiUrl.customer.addCustomer.format(data.CompanyId,data.MemberId);
            $http.post(url,data).success(function(response){
                deferred.resolve(response);
            }).error(function(err){
                deferred.reject(err)
            });
            return deferred.promise;
        }
        /**
         * 編輯客户
         * */
        function _exitCustomer(data){
            var deferred = $q.defer();
            var url = globalConfig.apiUrl.customer.exitCustomer.format(data.CompanyId,data.MemberId,data.CustomerId);
            $http.put(url,data).success(function(response){
                deferred.resolve(response);
            }).error(function(err){
                deferred.reject(err)
            });
            return deferred.promise;
        }
        return serviceFactory;
    }]);
});
