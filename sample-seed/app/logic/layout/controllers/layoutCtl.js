/**
 * Created by jianbo on 2016/2/25.
 */
/**

 * Created by mybbcat on 2015/12/8.
 * 模板页controller

 */

'use strict';

define(['app','logic/company/companyService.js'],function (app) {

    app.controller('layoutController',control);
    control.$inject = ['globalConfig','$scope','$state','cryptoJS','currentUserService','companyService'];

    function control(globalConfig,$scope,$state,cryptoJS,currentUserService,companyService){
        $scope.IsAdmin = ''||false;
        init();
        function init() {
            currentUserService.currentUser().then(function(currentUser){
                /*获取基本信息*/
                $scope.userInfo = currentUser;
                /*判断是否是管理员*/
                angular.forEach(currentUser.roleList,function(val,key){
                    if(val.type==0){
                        /*根据IsAdmin显示管理员菜单*/
                        $scope.IsAdmin = true;
                    }
                });
            },_errer);
        }
        var signIn_Success = function(){
            $state.go('home.dashboard');
        };

        var _errer = function(err){
            if (err) {
                $scope.viewBag.error = err.message.join('<br/>');
            }
        }

    };
});
