/**
 * Created by mybbcat on 2015/12/9.
 */
"use strict";

/**
 * 格式化字符串
 * eg: "xxx{0}xxx{1}".format("zz","vv")
 * @returns {string}
 */
String.prototype.format = function() {
    var args = arguments;
    return this.replace(/\{(\d+)\}/g, function(){
        var val = args[arguments[1]];
        return (! val) ? arguments[0] : val;
    });
};
/*整理时间格式
* eg: "/Datexxx/".toDate()
* @returns {string}
* */
String.prototype.toDate = function() {
    var s = this;
    var tmp = /\d+(?=\+)/.exec(s);
    var d = new Date(+tmp);
    if(d.getMonth()+1<10){
        var month = '0'+(d.getMonth()+1);
    }else{
        var month = d.getMonth()+1;
    }
    if(d.getDate()<10){
        var date = '0'+d.getDate();
    }else{
        var date = d.getDate();
    }
    var D = d.getFullYear()+'年'+month+'月'+date+'日';
    return D;
}
