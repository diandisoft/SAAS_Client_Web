/**
 * Created by mybbcat on 2015/10/14.
 */
require.config({
    paths:{
        'angular':'public/bower_components/angular/angular',
        'bootstrap':'public/bower_components/angular-bootstrap/ui-bootstrap-tpls',
        "angular-ui-router":"public/bower_components/angular-ui-router/release/angular-ui-router",
        "angular-async-loader":"public/bower_components/angular-async-loader/angular-async-loader",
        'angular-animate':'public/bower_components/angular-animate/angular-animate',
        'ngstorage':'public/bower_components/ngstorage/ngStorage',
        "angular-route-styles":"public/bower_components/angular-route-styles/ui-route-styles",
        "angular-validation": "public/bower_components/angular-validation/dist/angular-validation",
        "angular-validation-rule": "public/bower_components/angular-validation/dist/angular-validation-rule",
        "crypto":"public/bower_components/crypto-js/crypto-js",
        "underscore":"public/bower_components/underscore/underscore",
        "screenfull": "public/bower_components/screenfull/dist/screenfull",
        "animate.css":"public/bower_components/animate.css/animate.css",
        "angular-progress-button-styles":"public/bower_components/angular-progress-button-styles/dist/angular-progress-button-styles",
        "angularBootstrapNavTree":"public/bower_components/angular-bootstrap-nav-tree/dist/abn_tree_directive",
        "angular-xeditable":"public/bower_components/angular-xeditable/dist/js/xeditable",
        "angular-cache":"public/bower_components/angular-cache/dist/angular-cache",
        "angular-loading-bar":"public/bower_components/angular-loading-bar/src/loading-bar",
        "angular-ui-select":"public/bower_components/ui-select/dist/select",
        "ng-img-crop":"public/bower_components/ng-img-crop/compile/minified/ng-img-crop",

        "jquery": "public/bower_components/jquery/dist/jquery",

        "service":"modules/services",
        "directive":"modules/directives",
        "app":"modules/app/app.export",
        "app.config":"modules/app/app.config",
        "app.route":"modules/app/app.route",
        "app.run":"modules/app/app.run",

        "plupload": "public/bower_components/Plupload/js/plupload.full.min",

        "toolkit": "public/scripts/toolkit"
    },
    shim:{
        'angular': {exports: 'angular'},
        'angular-ui-router': {deps: ['angular']},
        'bootstrap':{deps:['angular']},
        'angular-async-loader':{deps:['angular']},
        'angular-animate':{deps:['angular']},
        'ngstorage':{deps:['angular']},
        "angular-route-styles": {deps:['angular']},
        "screenfull": {exports: "screenfull"},
        "angular-validation": {deps:['angular']},
        "angular-validation-rule": {deps:['angular','angular-validation']},
        "angular-progress-button-styles":{deps:['angular']},
        "angularBootstrapNavTree":{deps:['angular']},
        "angular-xeditable":{deps:['angular']},
        "angular-cache":{deps:['angular']},
        "angular-loading-bar":{deps:['angular','angular-animate']},
        "angular-ui-select":{deps:['angular']},
        "ng-img-crop":{deps:['angular']}
    },
    deps:['bootstrap'],
    urlArgs:"bust="+(new Date()).getTime()      //放置读取缓存，调试用
});

require(['jquery','require','angular','app.run'], function ($,require,angular) {
    $(document).ready(function () {
        angular.bootstrap(document, ['ddSaasApp']);
        $(document).find('html').addClass('ng-app:ddSaasApp');
    });
});

