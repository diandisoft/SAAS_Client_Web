/**
 * Created by mybbcat on 2015/10/19.
 * 注意：无需登陆即可访问的路由，必须增加以下路由属性："needLogin: false"
 */
define(['app'],function (app) {

    app.config(['$stateProvider', '$urlRouterProvider',function ($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise('/access/signin');

        //<editor-fold desc="账户相关的路由">
        $stateProvider
            .state('access',{
                url: '/access',
                template: '<div ui-view class="fade-in-right-big smooth"></div>'
            })
            .state('access.signin', {
                //成员登录
                url: '/signin',
                templateUrl: 'logic/account/views/signin.html',
                controllerUrl: 'logic/account/controllers/signinCtl.js',
                controller: 'signinController',
                needLogin: false
                /*
                 controller as
                 load more controllers, services, filters, ...
                 dependencies: ['services/authService']

                 load css
                 css:'public/styles/main.css'
                 css:['main1.css','main2.css']
                 */
            })
            .state('access.getCompanySimpleInfor',{
                //注册成员第一步，输入申请公司账号
                url: '/getCompanySimpleInfor',
                templateUrl: 'logic/account/views/companySimpleInfor.html',
                controllerUrl: 'logic/account/controllers/companySimpleInforCtl.js',
                controller: 'companySimpleInforController',
                needLogin: false
            })
            .state('access.signupUser',{
                //注册成员第二步，输入个人信息
                url: '/signupUser/:data',
                templateUrl: 'logic/account/views/signupUser.html',
                controllerUrl: 'logic/account/controllers/signupUserCtl.js',
                controller: 'signupController',
                needLogin: false
            })
            .state('access.signupCompany',{
                //注册新的公司
                url: '/signupCompany',
                templateUrl: 'logic/account/views/signupCompany.html',
                controllerUrl: 'logic/account/controllers/signupCompanyCtl.js',
                controller: 'signupCompanyController',
                needLogin: false
            })
            .state('access.resetInfo',{
                //被邀请用户首次登录重设个人信息
                url: '/resetInfo',
                templateUrl: 'logic/account/views/resetInfo.html',
                controllerUrl: 'logic/account/controllers/resetInfoCtl.js',
                controller: 'resetInfoController',
                needLogin: false
            })
            .state('access.forgetPassword',{
                //忘记密码，找回密码
            });
        //</editor-fold>

        //<editor-fold desc="成功登录后的功能业务相关路由">
        $stateProvider
            .state('home',{
                abstract: true,
                url: '/home',
                views : {
                    '':{
                        templateUrl : 'logic/layout/views/layout.html',
                        controllerUrl: 'logic/layout/controllers/layoutCtl.js',
                        controller: 'layoutController'
                    },
                    header : {
                        templateUrl : 'logic/layout/views/header.html'
                    },
                    miniChat : {
                        templateUrl : 'logic/layout/views/miniChat.html'
                    },
                    leftSide : {
                        templateUrl : 'logic/layout/views/leftSide.html'
                    }
                }
            })
            .state('home.dashboard', {
                url: '/dashboard',
                templateUrl : 'logic/dashBoard/views/dashBoard_v1.html',
                controllerUrl : 'logic/dashBoard/controllers/dashBoardCtl.js',
                controller : 'dashBoardController'
            })
            .state('home.setting', {
                url: '/individual',
                views : {
                    '':{
                        templateUrl : 'logic/company/views/individual.html',
                        controllerUrl : 'logic/company/controllers/individualCtl.js',
                        controller : 'individualController'
                    },
                    'exitInfo@home.setting' : {
                        templateUrl : 'logic/company/views/individualExitInfo.html',
                         controllerUrl : 'logic/company/controllers/individualExitInfoCtl.js',
                         controller : 'individualExitInfoController'
                    },
                    'setIcon@home.setting' : {
                        templateUrl : 'logic/company/views/individualSetIcon.html',
                        controllerUrl : 'logic/company/controllers/individualSetIconCtl.js',
                        controller : 'individualSetIconController'
                    }
                }
            })
            .state('home.uiSetting', {
                url: '/setting',
                templateUrl : 'logic/layout/views/uiSetting.html'
            })
        ;
        //<editor-fold desc="成员首页及布局的路由">
        $stateProvider
            .state('admin',{
                abstract: true,
                url: '/admin',
                views : {
                    '':{
                        templateUrl : 'logic/layout/views/layout.html',
                        controllerUrl: 'logic/layout/controllers/layoutCtl.js',
                        controller: 'layoutController'
                    },
                    header : {
                        templateUrl : 'logic/layout/views/header.html'/*,
                         controllerUrl : 'controllers/layout_header.js',
                         controller : 'headerCtl'*/
                    },
                    miniChat : {
                        templateUrl : 'logic/layout/views/miniChat.html'
                    },
                    leftSide : {
                        templateUrl : 'logic/layout/views/leftSide.html'
                    }
                }
            })
            .state('admin.departmentList',{
                url:'/departmentList',
                templateUrl:'logic/company/views/departmentList.html',
                controllerUrl: 'logic/company/controllers/departmentCtl.js',
                controller: 'DepartmentController'
            })
            .state('admin.member',{
                url:'/member/:data',
                templateUrl:'logic/company/views/member.html',
                controllerUrl: 'logic/company/controllers/memberCtl.js',
                controller: 'MemberController'
            })
            .state('admin.member.info',{
                url:'/info',
                templateUrl:'logic/company/views/memberInfo.html',
                controllerUrl: 'logic/company/controllers/memberInfoCtl.js',
                controller: 'MemberInfoController'
            })
            .state('admin.roleList',{
                url:'/roleList',
                templateUrl:'logic/company/views/roleList.html',
                controllerUrl: 'logic/company/controllers/roleListCtl.js',
                controller: 'RoleController'
            })
            .state('admin.role',{
                url:'/role/:RoleId',
                templateUrl:'logic/company/views/role.html'
            })
            .state('admin.role.roleMemberList',{
                url:'/roleMemberList',
                templateUrl:'logic/company/views/roleMemberList.html',
                controllerUrl: 'logic/company/controllers/roleMemberListCtl.js',
                controller: 'RoleMemberListController'
            })
            .state('admin.customer',{
                url:'/customer',
                template: '<div ui-view></div>'
            })
            .state('admin.customer.myCustomerList',{
                url:'/myCustomerList',
                templateUrl:'logic/customer/views/myCustomerList.html',
                controllerUrl: 'logic/customer/controllers/myCustomerListCtl.js',
                controller: 'MyCustomerListController'
            });
        //</editor-fold>

    }]);
});