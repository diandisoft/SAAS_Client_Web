/**
 * Created by mybbcat on 2015/10/19.
 */
define(['app'],function (app) {

    /**
     * 全局配置常量
      */
    app.constant('globalConfig',{
        version: Date.now()
    });

    /**
     * 向HTTP服务中注入令牌拦截服务
     */
    app.config(function ($httpProvider) {
        $httpProvider.interceptors.push('tokenInterceptorService');
    });

    /**
     * 设置angular-validation的参数
     */
    app.config(['$validationProvider', function($validationProvider) {
        $validationProvider.showSuccessMessage = false; // or true(default)
        //$validationProvider.setValidMethod('blur');
    }]);
});