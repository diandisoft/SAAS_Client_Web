'use strict';


define(function (require, exports, module) {
    var angular = require('angular');
    var asyncLoader = require('angular-async-loader');
    require('angular-ui-router');
    require('ngstorage');
    require('bootstrap');
    require('angular-route-styles');
    require('angular-validation');
    require('angular-validation-rule');
    require('angular-progress-button-styles');
    require('angularBootstrapNavTree');
    require('angular-animate');
    require('angular-xeditable');
    require('angular-cache');
    require('angular-loading-bar');
    require('angular-ui-select');
    require('ng-img-crop');

    var app = angular.module('ddSaasApp',
        ['ui.router','ngStorage','ui.bootstrap','routeStyles','validation','validation.rule','angular-progress-button-styles','angularBootstrapNavTree','ngAnimate','ui.select','xeditable','angular-cache','angular-loading-bar','ui.select','ngImgCrop']);

    asyncLoader.configure(app);

    module.exports = app;
});
