/**
 * Created by mybbcat on 2015/10/19.
 */
define(['app',
        'service/tokenInterceptorService',
        'service/venderService',
        'angular-animate',
        'app.config',
        'app.route',
        'service/authService',
        'directive/ui-global-prompt',
        'directive/ui-nav',
        'directive/ui-toggleclass',
        'directive/ui-fullscreen',
        'directive/ui-identifying-code',
        'directive/ui-head-portrait',
        'toolkit',
        'plupload',
        "service/currentUserService",
        "service/dictionaryService",
        "service/UISettingService"
    ],function (app) {

    app.run(['globalConfig','$rootScope','$location', '$window','$localStorage','authService','$state', '$stateParams','underscoreJS','UISettingService',
        function(globalConfig,$rootScope,$location, $window,$localStorage,authService,$state, $stateParams, underscoreJS,UISettingService){

            globalConfig.rootScope = $rootScope;

            init();
            initApiUrl();
            function init(){
                $rootScope.isLoading = false;
                $rootScope.app = {
                    name : '应用名称',
                    title : 'diandi saas platform.',
                    description : 'by Diandi technologies.',
                    keywords : '',
                    version : "0.0.1"
                };

                $rootScope.$state = $state;
                $rootScope.$stateParams = $stateParams;

                /*成员列表初始条件*/
                $rootScope.paramMemberCondition = {
                    CompanyId:" ",
                    Name:" ",
                    Phone:" ",
                    Position:" ",
                    RoleId:" ",
                    DepartmentId:" ",
                    Status:"2",
                    IsDimission:"false",
                    SortOrder:"0",
                    CurrentPage:1,
                    PageSize:20
                };
                if (angular.isDefined($localStorage.paramMemberCondition) ) {
                    $rootScope.paramMemberCondition = $localStorage.paramMemberCondition;
                } else {
                    $localStorage.paramMemberCondition = $rootScope.paramMemberCondition;
                }
            };

            /**
             * 全局 Web Api 的路径定义
             */
            function initApiUrl(){
                /*var webApiAuth = 'http://auth.my.me';
                var webApiCompany = 'http://company.my.me';*/
                var webApiAuth = 'http://192.168.1.108:2111';
                var webApiCompany = 'http://192.168.1.108:3111';
                var webApiCustomer = 'http://192.168.1.108:4111';

                globalConfig.apiUrl = {
                    token: webApiAuth + '/authentication/token',   // 用户验证
                    // 账号相关的API
                    account: {
                        getSmsSecurityCode: webApiCompany + "/NonBusiness/SmsCode/{0}?code={1}",
                        registerCompany: webApiCompany + "/Company",
                        registerMember:webApiCompany+"/Company/{0}/Member",
                        exitMember:webApiCompany+"/Company/{0}/Member/{1}",
                        getMyInfo:webApiCompany+"/Member/me"
                    },
                    //公司管理的API
                    company:{
                        getDepartmentList:webApiCompany+"/Company/{0}/department/tree",
                        getCompanySimpleInfor:webApiCompany+"/Company/{0}/simpleInfo",
                        getCompanyMemberList:webApiCompany+"/Company/{0}/Member/list",
                        comfirmMember:webApiCompany+"/CompanyAdmin/{0}/Member/{1}/Comfirm",
                        getRoleList:webApiCompany+"/Company/{0}/Role/List",
                        getRole:webApiCompany+"/Company/{0}/Role/{1}",
                        setDimission:webApiCompany+"/CompanyAdmin/{0}/Member/Dimission",
                        addMember:webApiCompany+"/CompanyAdmin/{0}/Member",
                        exitMember:webApiCompany+"/Company/{0}/Member/{1}",
                        paramMember:webApiCompany+"/Company/{0}/Member/{1}",
                        exitMemberRole:webApiCompany+"/CompanyAdmin/{0}/Member/{1}/Role",
                        sendSMS:webApiCompany+"/Company/{0}/Member/{1}/sms/Invite",
                        transferDepartment:webApiCompany+"/CompanyAdmin/{0}/department/{1}/Transfer",
                        transferDepartmentForOne:webApiCompany+"/CompanyAdmin/{0}/Member/{1}/department",
                        exitDepartment:webApiCompany+"/CompanyAdmin/{0}/department/{1}",
                        getDepartmentInfo:webApiCompany+"/Company/{0}/department/{1}",
                        addDepartment:webApiCompany+"/CompanyAdmin/{0}/department",
                        deleteDepartment:webApiCompany+"/CompanyAdmin/{0}/department/{1}",
                        deleteMember:webApiCompany+"/CompanyAdmin/{0}/Member/{1}",
                        downloadCvs:webApiCompany+"/CompanyAdmin/{0}/Member/Cvs/Template",
                        sendCvs:webApiCompany+"/CompanyAdmin/{0}/Member/Cvs",
                        exportCvs:webApiCompany+"/ CompanyAdmin/{0}/Member/Cvs/Export",
                        dissolutionCompany:webApiCompany+"/Company/{0}",
                        setFunctionRange:webApiCompany+"/CompanyAdmin/{0}/Member/{1}/FunctionRange",
                        setMemberArchives:webApiCompany+"/CompanyAdmin/{0}/Member/{1}/Archives",
                        addRole:webApiCompany+"/CompanyAdmin/{0}/role",
                        exitRole:webApiCompany+"/CompanyAdmin/{0}/role/{1}",
                        deleteRole:webApiCompany+"/CompanyAdmin/{0}/role/{1}",
                        batchJoinRole:webApiCompany+"/CompanyAdmin/{0}/role/{1}/Join",
                        batchOutRole:webApiCompany+"/CompanyAdmin/{0}/role/{1}/Exit",
                        outCompany:webApiCompany+"/Company/{0}/Member/{1}",
                        getRoleFunction:webApiCompany+"/Company/{0}/role/{1}/Function",
                        setRoleFunction:webApiCompany+"/CompanyAdmin/{0}/role/{1}/Function",
                        getImagesCode:webApiCompany+"/ImagesCode/{0}?text={1}",
                        fileUpload:webApiCompany+"/File/Upload?type={0}"
                    },
                    customer:{
                        myCustomerList:webApiCustomer+"/Company/{0}/Member/{1}/Customer/List",
                        getCustomerInfo:webApiCustomer+"/Company/{0}/Member/{1}/Customer/{2}",
                        addCustomer:webApiCustomer+"/Company/{0}/Member/{1}/Customer",
                        exitCustomer:webApiCustomer+"/Company/{0}/Member/{1}/Customer/{2}"
                    }
                };
            }
            //<editor-fold desc="全局监听器">

            $rootScope.$on("$stateNotFound",function(){
               //TODO 找不到视图时触发该事件
                console.error('State Not Found!!!');
            });

            /**
             * 判断目标路由是否允许未经登陆进行访问
             */
            $rootScope.$on("$stateChangeStart", function(event, nextRoute, currentRoute) {
                if(nextRoute){
                    if(underscoreJS.isUndefined(nextRoute.needLogin) || nextRoute.needLogin){
                        if (!authService.authentication.isAuth) {
                            $state.go('access.signin');
                            event.preventDefault();
                        }
                    }
                }
            });

            $rootScope.$on('$locationChangeStart', function (event, next, current) {
                //刷新页面时，重新读取令牌，并写入app中
                authService.fillAuthData();
            });

            /**
             * 监视Memberdata，如发生变化时马上保存至本地session
             */
            $rootScope.$watch('paramMemberCondition', function(){
                $localStorage.paramMemberCondition = $rootScope.paramMemberCondition;
            },true);
            //</editor-fold>
        }]);
});