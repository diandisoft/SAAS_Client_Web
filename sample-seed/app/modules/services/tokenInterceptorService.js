/**
 * Created by mybbcat on 2015/10/14.
 * 认证令牌的拦截服务
 */
define(['app'],function (app) {

    app.factory('tokenInterceptorService',['$q','$window','$injector',"globalConfig",function ($q, $window, $injector,globalConfig) {
        return {
            /*
             修改请求正文
             */
            request: function (config) {
                config.headers = config.headers || {};

                //在请求中附带认证令牌
                var authData = $window.sessionStorage.getItem('authorizationData');
                if (authData) {
                    var _data = JSON.parse(authData);
                    config.headers.token = _data.token;
                }

                //todo 加载中全局提示

                return config;
            },

            requestError: function(config) {

                //todo 取消加载中全局提示
                return $q.reject(config);
            },

            /*
             获得响应对象，可以修改它
             */
            response: function (response) {

                //TODO 停止加载中全局提示

                return response || $q.when(response);
            },

            /* Revoke client authentication if 401 is received */
            responseError: function(response) {
                if (response != null){
                }
                switch (response.status){
                    case 401:
                        $injector.get('$state').transitionTo('access.signin');
                        break;
                    case -1:
                        //TODO 使用弹出提示框进行提示
                        console.warn("无法连接服务器！");
                        break;
                    case 405:
                        //TODO 使用弹提示框进行提示
                        console.warn("服务器拒绝访问！");
                        break;
                    case 400||500:
                        console.warn(response.data.message.join(';'));
                        break;
                    default :
                        console.error("404");
                        break;
                }
                //todo 取消加载中全局提示
                return $q.reject(response);
            }
        };
    }]);
});