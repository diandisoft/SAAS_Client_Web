/**
 * Created by mybbcat on 2015/10/16.
 * 将第三方插件都注册成为Angular的服务，方便管理与注入
 */

define(['app','crypto',"underscore"],function (app,CryptoJS,_) {

    app.factory('cryptoJS', function () {
        return CryptoJS;
    });

    app.factory('underscoreJS', function () {
        return _;
    });
});