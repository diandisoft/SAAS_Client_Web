/**
 * author      : mybbcat
 * createTime  : 2016/2/2
 * description :
 */
define(['app'],function (app) {

    app.factory('dictionaryService',function () {
        var serviceFactory = {};
        /*功能权限*/
        var _functionPermissionCategory = {
            1:{Name:'盘客管理',Description:'房产业务管理功能权限，指盘客管理功能',Value:1},
            2:{Name:'无主盘客管理',Description:'企业内发通知的功能权限',Value:2},
            3:{Name:'导出盘客数据',Description:'导出房产经纪数据的功能权限 例如导出盘客数据、统计数据等',Value:3},
            4:{Name:'浏览盘客敏感信息',Description:'浏览非职权范围内或浏览其他人的房产敏感数据的功能权限 值房产的栋座、单元、房号、具体楼层',Value:4},
            5:{Name:'管理成交报告',Description:'成交报告管理功能权限',Value:5}
        };
        /*优先级*/
        var _level = {
            0:{Name:'Lower',Description:'最低',Value:0},
            1:{Name:'Low',Description:'低',Value:1},
            2:{Name:'Normal',Description:'中',Value:2},
            3:{Name:'High',Description:'高',Value:3},
            4:{Name:'Higher',Description:'最高',Value:4}
        };
        /*性别*/
        var _gender = {
            0:{Name:'male',Description:'男',Value:0},
            1:{Name:'female',Description:'女',Value:1}
        };
        /*客户状态*/
        var _customerStatus = {
            0:{Name:'Normal',Description:'正常',Value:0},
            1:{Name:'Invalid',Description:'失效',Value:1}
        };
        /*上传文件目录*/
        var _fileDirectory = {
            0:{Name:'Common',Description:'公共',Value:0},
            1:{Name:'Company',Description:'公司',Value:1},
            2:{Name:'Icon',Description:'头像',Value:2},
            3:{Name:'Demand',Description:'需求',Value:3}
        }
        serviceFactory.FunctionPermissionCategory = _getFunctionPermissionCategory;
        serviceFactory.Level = _getLevel;
        serviceFactory.Gender = _getGender;
        serviceFactory.CustomerStatus = _getCustomerStatus;
        serviceFactory.FileDirectory = _getFileDirectory;
        /*SAAS平台的功能权限定义*/
        function _getFunctionPermissionCategory(){
            return _functionPermissionCategory;
        }
        /*获取客户优先级*/
        function _getLevel(){
            return _level;
        }
        /*获取性别*/
        function _getGender(){
            return _gender;
        }
        /*获取客户状态*/
        function _getCustomerStatus(){
            return _customerStatus;
        }
        /*获取上传文件目录*/
        function _getFileDirectory(){
            return _fileDirectory;
        }
        return serviceFactory;
    });
});