/**
 * Created by jianbo on 2016/3/8.
 * run.js引用接口UISettingService即可使用
 */
define(['app'],function (app) {

    app.factory('UISettingService',['$rootScope','$localStorage', function ($rootScope,$localStorage) {
        var serviceFactory = {};
        $rootScope.ui = {
            themeID: 1,
            color: {
                navbarHeaderColor: 'bg-black',
                navbarCollapseColor: 'bg-white-only',
                asideColor: 'bg-black'
            },
            layout: {
                headerFixed: true,
                asideFixed: false,
                asideFolded: false,
                asideDock: false,
                //固定居中显示或全屏显示
                container: false
            }
        };
        // 将UI配置保存在本地
        if (angular.isDefined($localStorage.ui)) {
            $rootScope.ui = $localStorage.ui;
        } else {
            $localStorage.ui = $rootScope.ui;
        }

        /**
         * 监视界面UI的个人设置，如发生变化时马上保存至本地session
         */
        $rootScope.$watch('ui', function () {
            if ($rootScope.ui.layout.asideDock && $rootScope.ui.layout.asideFixed) {
                // aside dock and fixed must set the header fixed.
                $rootScope.ui.layout.headerFixed = true;
            }
            $localStorage.ui = $rootScope.ui;
        }, true);
        return serviceFactory;
    }]);
});