/**
 * Created by mybbcat on 2015/10/14.
 * 账号认证服务
 */
define(['app'],function (app) {

    app.factory('authService',['globalConfig','$http','$q','$window','$state',function(globalConfig,$http,$q,$window,$state) {

        var authServiceFactory = {};

        var _authentication = {
            isAuth :false,
            userName : ''
        };

        authServiceFactory.signIn = _signIn;
        authServiceFactory.signOut = _signOut;
        authServiceFactory.fillAuthData = _fillAuthData;
        authServiceFactory.authentication = _authentication;

        /*进行登录*/
        function _signIn(username, password) {
            var deferred = $q.defer();
            $http.post(globalConfig.apiUrl.token,{account: username, password: password})
                .success(function(response){
                     var _data = {token:response.token,userName:username};
                    $window.sessionStorage.setItem('authorizationData',JSON.stringify(_data));

                    _authentication.isAuth = true;
                    _authentication.userName = username;

                    deferred.resolve(response);
                }).error(function(err,status){
                    _signOut();
                    deferred.reject(err)
                });
            return deferred.promise;
        };

        /*退出登录*/
        function _signOut(){
            $window.sessionStorage.removeItem('authorizationData');
            _authentication.isAuth = false;
            _authentication.userName = '';

            $state.go('access.signin');
        };

        //初始化APP时，或刷新APP时，重新去本地session中获取认证数据，检查是否已经通过认证
        function _fillAuthData(){
            var authData = $window.sessionStorage.getItem('authorizationData');
            if(authData){
                _authentication.isAuth = true;
                _authentication.userName = authData.userName;
            }
        };

        return authServiceFactory;
    }]);

});