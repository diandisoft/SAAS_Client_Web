/**
 * Created by mybbcat on 2016/2/2.
 * 获取当前登录用户的个人与组织信息，包括当前用户的编号、姓名、功能权限等等
 */

define(['app'],function (app) {

    app.factory('currentUserService',['globalConfig','$http','CacheFactory','$q', function (globalConfig,$http,CacheFactory,$q) {
        //这里利用第三方的缓存插件
        CacheFactory('dataCache', {
            maxAge: 15 * 60 * 1000, // Items added to this cache expire after 15 minutes
            cacheFlushInterval: 60 * 60 * 1000, // This cache will clear itself every hour
            deleteOnExpire: 'aggressive' // Items will be deleted from this cache when they expire
        });
        var serviceFactory = {};
        var _currentUser = {
            id: '',
            company: {
                id: '',
                name: ''
            },
            name: '',
            phone: '',

            departmentList: {
                name: ''
            },
            roleList: {
                name: ''
            },
            firstlogin:'',
            functionPermission: {

            }
        };

        serviceFactory.currentUser = _getCurrentUser;

        function _getCurrentUser(){
            var deferred = $q.defer();
            var url = globalConfig.apiUrl.account.getMyInfo;
            var start = new Date().getTime();
            $http.get(url,{
                cache: CacheFactory.get('dataCache')
            }).success(
                function(data) {
                    console.log('time taken for request: ' + (new Date().getTime() - start) + 'ms');
                    _success(data,deferred);
                },function(){
                    _error(deferred);
                });
            return deferred.promise;
        }

        function _success(response,deferred){
            _currentUser.id = response.memberinfo.id;
            _currentUser.company.id = response.memberinfo.companyId;
            _currentUser.name = response.memberinfo.name;
            _currentUser.phone = response.memberinfo.tel1;
            _currentUser.departmentList = response.memberinfo.departmentList;
            _currentUser.roleList = response.memberinfo.roleList;
            _currentUser.firstlogin = response.firstlogin;
            deferred.resolve(_currentUser);
        }

        function _error(deferred){
            deferred.reject(_currentUser);
        }

        return serviceFactory;
    }]);

});