/**
 * Created by jianbo on 2016/3/7.
 * 获取头像指令
 * @setClass 所需的class，如img-circle，可选
 * 代用方法：<div ui-head-portrait setClass="@class"></div>
 */
define(['app','logic/company/companyService.js'],function (app) {

    app.directive('uiHeadPortrait',uiHeadPortrait);
    uiHeadPortrait.$inject = ['globalConfig', 'currentUserService','companyService','dictionaryService'];

    function uiHeadPortrait (globalConfig, currentUserService,companyService,dictionaryService) {
        return {
            restrict: 'EA',
            template:'<img src="{{myIconUrl}}" class="{{setClass}}"  onerror=\"this.src=\'public/images/a0.jpg\'\"/>',
            link: function($scope,tElement,tAttrs) {
                $scope.setClass = tAttrs.setclass||'';
                globalConfig.rootScope.myIconUrl = '';
                /*重新获取头像*/
                globalConfig.rootScope.getHeadPortrait = _getHeadPortrait;
                /*获取头像*/
                _getHeadPortrait();
                function _getHeadPortrait() {
                    currentUserService.currentUser().then(function (currentUser) {
                        var fileDirectory = dictionaryService.FileDirectory();
                        companyService.getFileUpload(fileDirectory[2].Value).then(function (data) {
                            globalConfig.rootScope.myIconUrl = data.host + '/icon/' + currentUser.company.id + '/' + currentUser.id + '.jpg?' + Math.random();
                        });
                    });
                }
            }
        };
    };
});