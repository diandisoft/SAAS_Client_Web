/**
 * Created by jianbo on 2016/3/7.
 * 验证码指令
 * 代用方法：<div ui-identifying-code></div>
 */
define(['app','logic/company/companyService.js'],function (app) {

    app.directive('uiIdentifyingCode',uiCode);
    uiCode.$inject = ['globalConfig', 'currentUserService','companyService'];

    function uiCode (globalConfig, currentUserService,companyService) {
        return {
            restrict: 'A',
            template:'<div class="hbox">' +
            '<div class="col"><input type="text" class="form-control" placeholder="请输入验证码" ng-model="form.code" required validator="required,maxlength=4,minlength=4"></div>' +
            '<div class="col" style="width: 90px;"><img src="{{identifyingCode}}" ng-click="getIdentifyingCode()" height="34px" class="b-r b-b b-t"/></div>' +
            '</div>',
            link: function($scope) {
                /*点击验证码切换*/
                $scope.getIdentifyingCode = _getIdentifyingCode;
                /*初始获取验证码*/
                _getIdentifyingCode();
                /*获取验证码*/
                function _getIdentifyingCode() {
                    currentUserService.currentUser().then(function (data) {
                        companyService.getImagesCode(data.phone).then(
                            function (respon) {
                                $scope.identifyingCode = respon;
                            }, _getCodeFail
                        );
                    }, _getCodeFail);
                }
                function _getCodeFail(err){
                    if(err) {
                        globalConfig.rootScope.successMessage = '';
                        globalConfig.rootScope.errorMessage = err.message.join('&lt;br&frasl;&gt;');
                    }
                }
            }
        };
    };
});