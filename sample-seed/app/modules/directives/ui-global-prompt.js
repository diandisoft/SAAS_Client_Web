/**
 * Created by jianbo on 2016/3/7.
 * 提示指令
 * 代用方法：<div ui-global-prompt></div>
 */
define(['app'],function (app) {

    app.directive('uiGlobalPrompt',uiGlobalPrompt);
    uiGlobalPrompt.$inject = ['globalConfig', '$anchorScroll'];

    function uiGlobalPrompt (globalConfig, $anchorScroll) {
        return {
            restrict: 'A',
            template:'<div class="text-danger col v-bottom" ng-show="errorMessage">{{errorMessage}}</div>' +
            '<div class="text-success col v-bottom" ng-show="successMessage">{{successMessage}}</div>',
            link: function() {
                globalConfig.rootScope.errorMessage = '';
                globalConfig.rootScope.successMessage = '';
            }
        };
    };
});