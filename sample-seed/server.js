/**
 * Created by mybbcat on 2015/10/14.
 */
var express = require('express');
var app = express();

app.use('/', express.static(__dirname + '/app'));
app.use('/assets', express.static(__dirname + '/node_modules'));

app.listen(8081, function() {
    console.log('listening on http://localhost:8081/');
});